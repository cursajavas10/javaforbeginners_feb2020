import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;
import javax.swing.JTable;

public class EmployeeApp {

	private JFrame frame;
	private JTextField textFieldName;
	private JTextField textFieldSurname;
	private JTextField textFieldSalary;
	JSpinner spinner = new JSpinner();
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EmployeeApp window = new EmployeeApp();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public EmployeeApp() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 318);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JLabel lblName = new JLabel("Name:");
		lblName.setBounds(20, 11, 46, 14);
		frame.getContentPane().add(lblName);

		JLabel lblSurname = new JLabel("Surname:");
		lblSurname.setBounds(20, 48, 46, 14);
		frame.getContentPane().add(lblSurname);

		JLabel lblAge = new JLabel("Age:");
		lblAge.setBounds(202, 11, 46, 14);
		frame.getContentPane().add(lblAge);

		JLabel lblSalary = new JLabel("Salary:");
		lblSalary.setBounds(202, 48, 46, 14);
		frame.getContentPane().add(lblSalary);

		textFieldName = new JTextField();
		textFieldName.setBounds(61, 8, 86, 20);
		frame.getContentPane().add(textFieldName);
		textFieldName.setColumns(10);

		textFieldSurname = new JTextField();
		textFieldSurname.setBounds(76, 45, 86, 20);
		frame.getContentPane().add(textFieldSurname);
		textFieldSurname.setColumns(10);

		spinner.setModel(new SpinnerNumberModel(new Integer(18), new Integer(18), null, new Integer(1)));
		spinner.setBounds(244, 8, 40, 20);
		frame.getContentPane().add(spinner);

		textFieldSalary = new JTextField();
		textFieldSalary.setBounds(254, 45, 86, 20);
		frame.getContentPane().add(textFieldSalary);
		textFieldSalary.setColumns(10);

		JButton btnInsert = new JButton("Insert");
		btnInsert.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String name = textFieldName.getText();
				String surname = textFieldSurname.getText();
				int age = (int) spinner.getValue();
				String salaryTxt = textFieldSalary.getText();

				insertEmployee(name, surname, age, Integer.parseInt(salaryTxt));
			}
		});
		btnInsert.setBounds(295, 81, 89, 23);
		frame.getContentPane().add(btnInsert);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(20, 123, 278, 111);
		frame.getContentPane().add(scrollPane);

		table = new JTable();
		scrollPane.setViewportView(table);

		JButton btnSelect = new JButton("Get Employees");
		btnSelect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				getEmployees();
			}
		});
		btnSelect.setBounds(144, 245, 154, 23);
		frame.getContentPane().add(btnSelect);
	}

	private void getEmployees() {
		try {
			DefaultTableModel model = new DefaultTableModel();
			model.addColumn("Name");
			model.addColumn("Surname");
			model.addColumn("Age");
			model.addColumn("Salary");

			Class.forName("com.mysql.jdbc.Driver");
			Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/dbemployee", "root",
					"root");
			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery("select * from employee;");

			while (rs.next()) {
				System.out.println(rs.getInt(1) + " " + rs.getString("name") + " " + rs.getDouble("salary"));
				
				Object[] row = new Object[4];
				row[0] = rs.getString("name");
				row[1] = rs.getString("surname");
				row[2] = rs.getInt("age");
				row[3] = rs.getDouble("salary");
				
				model.addRow(row);
			}
			
			table.setModel(model);

			connection.close();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Database Connection error!");
			e.printStackTrace();
		}
	}

	private void insertEmployee(String name, String surname, int age, int salary) {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/dbemployee", "root",
					"root");
			Statement stmt = connection.createStatement();

			stmt.executeUpdate("INSERT INTO `dbemployee`.`employee` (`name`, `age`, `surname`, `salary`) " + "VALUES ('"
					+ name + "', '" + age + "', '" + surname + "', '" + salary + "');");

			connection.close();
			JOptionPane.showMessageDialog(null, "Insert with success!");
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Database Connection error!");
			e.printStackTrace();
		}
	}
}
