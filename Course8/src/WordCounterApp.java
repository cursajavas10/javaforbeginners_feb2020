import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JRadioButton;
import javax.swing.JToggleButton;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.JEditorPane;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JSlider;
import javax.swing.JProgressBar;
import java.awt.Toolkit;
import javax.swing.UIManager;
import javax.swing.ImageIcon;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.awt.event.ActionEvent;

public class WordCounterApp {

	private JFrame frmWordCounter;
	JTextArea textArea = new JTextArea();
	JLabel lblCharacters = new JLabel("Characters:");
	JLabel lblWords = new JLabel("Words:");

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					WordCounterApp window = new WordCounterApp();
					window.frmWordCounter.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public WordCounterApp() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmWordCounter = new JFrame();
		frmWordCounter.getContentPane().setBackground(UIManager.getColor("activeCaption"));
		frmWordCounter.setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\Vlad\\Desktop\\pachet-din-lemn-cu-26-litere-magnetice-din-lemn-copie-395-5691.png"));
		frmWordCounter.setTitle("Word Counter");
		frmWordCounter.setBounds(100, 100, 450, 300);
		frmWordCounter.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmWordCounter.getContentPane().setLayout(null);
		
		
		lblCharacters.setBounds(24, 11, 113, 14);
		frmWordCounter.getContentPane().add(lblCharacters);
		
		
		lblWords.setBounds(24, 36, 92, 14);
		frmWordCounter.getContentPane().add(lblWords);
		
		
		textArea.setBounds(22, 61, 402, 147);
		frmWordCounter.getContentPane().add(textArea);
		
		JButton btnClick = new JButton("Click");
		btnClick.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String text = textArea.getText();
				int noOfChars = text.length();
				lblCharacters.setText("Characters:" + noOfChars);
				
				String[] words = text.split(" ");
				int noOfWords = words.length;
				lblWords.setText("Words:" + noOfWords);
				
			}
		});
		btnClick.setFocusable(false);
		btnClick.setBorderPainted(false);
		btnClick.setBorder(null);
		btnClick.setBackground(new Color(0, 0, 255));
		btnClick.setForeground(new Color(255, 0, 0));
		btnClick.setIcon(null);
		btnClick.setBounds(27, 227, 89, 23);
		frmWordCounter.getContentPane().add(btnClick);
	}
	
}
