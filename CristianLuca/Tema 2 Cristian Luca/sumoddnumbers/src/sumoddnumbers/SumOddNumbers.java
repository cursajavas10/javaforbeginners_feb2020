package sumoddnumbers;

import java.util.Scanner;

public class SumOddNumbers {
	public static void main(String[] args) {
		
		int n, i, oddSum = 0;
		
		Scanner reader = new Scanner(System.in);
		
		System.out.print("Number of terms is: ");
		
		n = Integer.parseInt(reader.nextLine());
		
		System.out.print("The odd numbers are: ");
		
		for (i = 0; i < n; i++) {
			
			if(i % 2 == 1) {
				System.out.print(" " + i + " ");
				oddSum += i;
			}
			
		}
		
		System.out.println("\nThe Sum of odd Natural Number up to " + n + " terms is: " + oddSum);
		
		reader.close();
	}
}

