package asciivaluecharacter;

import java.util.Scanner;

public class AsciiValueCharacter {
	public static void main(String[] args) {
		
		System.out.print("Enter a character: ");
		
	    Scanner sc = new Scanner(System.in);
	    
	    char ch = sc.next().charAt(0);	
	    /*ideal ar fi fost sa verifici ce s-a introdus de la tastatura. Daca se introducea un String format din mai multe caractere? 
	    Puteai ori sa afisezi codul ascii pentru toate caracterele ori sa afisezi un mesaj de eroare. */
	    
	    int asciiValueCharacter = ch;
	    
	    System.out.println("ASCII value of character entered is " + asciiValueCharacter);
	}
}
