package litergascitytrip;

import java.util.Scanner;

public class LiterGasCityTrip {

	public static void main(String[] args) {
		
		float startKm, endKm, consCity, consHigh, result;
		byte inCity;
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Start of the car in km: ");
		startKm = sc.nextFloat();
		System.out.println("End of the car in km: ");
		endKm = sc.nextFloat();
		System.out.println("The amount of the gas in city: ");
		consCity = sc.nextFloat();
		System.out.println("The amount of the gas on highway: ");
		consHigh = sc.nextFloat();
		
		System.out.println("Is the trip in the city?");
		inCity = sc.nextByte();								// !!!! BYTE are 8 biti. Nu puteai folosi boolean pentru un singur bit?
		
		float startEndKm = endKm - startKm;
		
		if (inCity == 1) {
			result = startEndKm * consCity;
			System.out.println("The result is: " + result + " liter of gas in the city on the distance of " + startEndKm + " km");
		} else if (inCity == 0) {
			result = startEndKm * consHigh;
			System.out.println("The result is: " + result + " liter of gas on the highway on the distance of " + startEndKm + " km");
		}
		sc.close();
	}

}

//