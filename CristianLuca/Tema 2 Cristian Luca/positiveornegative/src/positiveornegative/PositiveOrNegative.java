package positiveornegative;

import java.util.Scanner;

public class PositiveOrNegative {

	public static void main(String[] args) {
			Scanner reader = new Scanner(System.in);
	        System.out.print("Type a number: ");
	        int num = Integer.parseInt(reader.nextLine());
	        
	        if (num > 0) {
	            System.out.println("The number is positive.");
	        }
	        else if (num < 0) { 
	            System.out.println("The number is negative.");
	        }															// NU ai tratat cazul cand numarul este 0
	       
	        reader.close();	
	}

}
