package patternrightangletriangle;

import java.util.Scanner;

public class PatternRightAngleTriangle {

	public static void main(String[] args) {
		
		int i, j, num, numRows; 
		
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Input number of rows: ");
		
		numRows = sc.nextInt();
	    
		System.out.println("\nLook at this beautiful pattern!\n");
		
        for(i = 0; i < numRows; i++) // outer loop for rows
        { 
            num = 1; 
            for(j = 0; j <= i; j++) // inner loop for rows
            { 
                // printing num with a space  
                System.out.print(num + " "); 
    
                //incrementing value of num 
                num++; 
            } 
    
            // ending line after each row 
            System.out.println(); 

	    }
        sc.close();
        
	}

}

// Nu prea am inteles mecanismul acestui cod! L-am copiat!			//Este un algoritm. Puteai sa construiesti oricare altul care functiona. Citeste codul de mai multe ori cap coada si vezi ce se intampla.