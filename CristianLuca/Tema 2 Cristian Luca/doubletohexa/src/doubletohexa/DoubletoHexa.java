package doubletohexa;

import java.util.Scanner;

public class DoubletoHexa {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Enter a decimal number: ");
		
		String str = sc.nextLine();
		
		//int nr = Integer.parseInt(str);
		
		double nr = Double.parseDouble(str);
		
		//String hex = Integer.toHexString(nr);
		
		String hex = Double.toHexString(nr);
		
		
		System.out.println("Hex value is " + hex);
		
		sc.close();
	}

}

