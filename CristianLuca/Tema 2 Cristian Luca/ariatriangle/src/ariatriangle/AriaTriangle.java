package ariatriangle;

import java.util.Scanner;

public class AriaTriangle {
	public static void main(String[] args) {
		
		double areaTriangle;
		
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Give me the base-width: ");
		
		double baseWidth = sc.nextDouble();
		
		System.out.print("Give me the height: ");
		
		double height = sc.nextDouble();
		
		areaTriangle = baseWidth * height / 2.0;
		
		String valueTriangle = String.format("two numbers after decimal is: %1$.2f",				//de ce te-ai dus pe urmatorul rand? Incapea pe un sigur rand totul
		        areaTriangle);
		
		//the "f" format will round the number up, not truncate digits.
		
		System.out.println("The area of triangle with " + valueTriangle);
	}
	

}

