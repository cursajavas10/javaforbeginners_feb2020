package lengthstring;

import java.util.Scanner;

public class LengthString {
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Give me the string: ");
		
		String s = sc.nextLine();
		
		
		int count = 0;
		
		char [] ca = s.toCharArray();
		
		for (char c : ca) {
			count++;
		}
		
		System.out.println(count);// ce spui despre s.length() ? Era mai usor asa.
	}
}
