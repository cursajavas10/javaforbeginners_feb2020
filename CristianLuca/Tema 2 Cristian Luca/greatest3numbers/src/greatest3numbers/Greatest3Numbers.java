package greatest3numbers;
import java.util.Scanner;
public class Greatest3Numbers {
	
	public static void main(String[] args) {
		
		int num1, num2, num3;
		Scanner sc = new Scanner(System.in);		
		System.out.println("Enter three numbers: ");
		num1 = sc.nextInt();
		num2 = sc.nextInt();
		num3 = sc.nextInt();
		
		if (num1 > num2 && num1 > num3) {
			System.out.format("%d is greater than both %d and %d", num1, num2, num3);
		}
		else if (num2 > num1 && num2 > num3) {
			System.out.format("%d is greater than both %d and %d", num2, num1, num3);
		}	
		else if (num3 > num1 && num3 > num2) {
			System.out.format("%d is greater than both %d and %d", num3, num1, num2);
		}		
		else {
			System.out.println("/n Either any two values or all the three values are equal");
		}
		
	}	
}
