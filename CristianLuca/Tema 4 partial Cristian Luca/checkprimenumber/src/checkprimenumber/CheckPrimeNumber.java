package checkprimenumber;

import java.util.Scanner;

public class CheckPrimeNumber {
	public static void main(String args[]) {
		int n, i, flag = 1; //TODO: flag could be a boolean type

		Scanner sc = new Scanner(System.in);

		try {
			System.out.println("Enter a positive integer number with maximum 10 digits for checking prime number: ");
			n = Integer.parseInt(sc.nextLine());
			if (n >= 2) {
				System.out.println(n + " is a valid positive integer number");

				for (i = 2; i <= n / 2; ++i) {
					if (n % i == 0) {
						flag = 0;	//TODO: flag could be a boolean type
						break;
					}
				}

				if (flag == 1) {
					System.out.println("It's a prime number.");
				} else {
					System.out.println("It's not a prime number.");
				}
			} else if (n == 1){
				System.out.println(n + " is a valid positive integer number");
				System.out.println("It's not a prime number.");
				
			} else if (n == 0) {
				System.out.println(n + " is not a valid positive integer number");
				System.out.println("It's not a prime number.");
			} else {
				System.out.println("It is not positive integer number. Run the program again!");
			}
			

		} catch (NumberFormatException e) {
			System.out.println("It is not a valid integer number. Run the program again!");
		}

	}
}
