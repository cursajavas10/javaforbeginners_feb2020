package converttobinary;

import java.util.Scanner;

public class ConvertToBinary {

	public static void main(String args[]) {	//ESTE BINE!
		System.out.println("Enter the number you want to convert: ");
		Scanner scanner = new Scanner(System.in);
		int num = scanner.nextInt();
		if (num > 0) {
			System.out.println("Binary conversion for " + num + " is:");
			convertToBinary(num);
		}
		scanner.close();
	}

	/*
	 * ALGHORITM: 
	 * 1. Divide the number by 2 using % operator and store the remainder somewhere.
	 * 2. Divide the number by 2 using / operator.
	 * 3. Repeat above two steps until number becomes 0.
	 *  4. Now print the remainders in reverse order.
	 */
	
	static void convertToBinary(int num) {
		int container[] = new int[100];
		int i = 0;
		while (num != 0) {
			container[i] = num % 2;
			i++;
			num = num / 2;
		}
		for (int j = i - 1; j >= 0; j--) {
			System.out.print(container[j]);
		}

	}

}
// OBS. Integer.toString() Method OR Integer.toBinaryString() Method are much easier;
