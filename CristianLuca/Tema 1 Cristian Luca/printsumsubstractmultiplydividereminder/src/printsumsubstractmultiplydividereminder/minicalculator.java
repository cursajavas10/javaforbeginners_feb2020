package printsumsubstractmultiplydividereminder;

import java.util.Scanner;
public class minicalculator {

	public static void main(String[] args) {
		System.out.println("welcome to the calculator");

		while (true) {
			Scanner reader = new Scanner(System.in);
		    System.out.print("Enter a command (sum, difference, product, quotient, reminder, quit): ");
		    String command = reader.nextLine();
		    if (command.equals("quit")) {
		        break;
		    }

		    System.out.print("enter the numbers");
		    double first = Double.parseDouble(reader.nextLine());	//vezi ca aici iti crapa aplicatia daca se introduce un numar care nu poate fi parsat.
		    double second = Double.parseDouble(reader.nextLine());

		    if (command.equals("sum") ) {
		        double sum = first + second;
		        System.out.println( "The sum of the numbers is " + sum );
		        reader.nextLine();
		    } else if (command.equals("difference")) {
		        double difference = first - second;
		        System.out.println("The difference of the numbers is " + difference);
		    } else if (command.equals("product")) {
		    	double product = first * second;
		    	System.out.println("The product of the numbers is " + product);
		    } else if (command.equals("quotient") && second != 0) {
		    	double quotient = first / second;
		    	System.out.println("The quotient of the numbers is " + quotient);
		    	 reader.nextLine();
		    } else if (command.equals("reminder")) {
		    	double reminder = first % second;
		    	System.out.println("The reminder of the numbers is " + reminder);
		    } else if (second == 0) {
		    	System.out.println("Error!");
		    } else {
		    	 System.out.println("Unknown command");
		    }
		System.out.println("Thanks, bye!");
		System.out.println();
		}
		// program inspired by finlandezi
	}
}
