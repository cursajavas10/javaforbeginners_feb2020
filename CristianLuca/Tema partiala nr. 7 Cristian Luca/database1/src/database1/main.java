package database1;

import database1.dbhandler;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class main { //TODO: incearca sa tratezi exceptiile cu mesaje frumoase pt user.

	static private dbhandler dbHandler;
	static private Connection connection;
	static private PreparedStatement preparedStatement;

	public static void main(String[] args) throws ClassNotFoundException, SQLException {

		dbHandler = new dbhandler();
		connection = dbHandler.getDbConnection();
		// System.out.println("SUCCESFULLY LOGIN TO THE DATABASE " +
		// connection.getCatalog());

		// writeToDB();

		readFromDB();

		// updateDB("Luca", 42, "Monica", 5000, 5);

		// deleteUser(6);
	}

	public static void writeToDB() throws SQLException {

		String insert = "INSERT INTO employee(name,age,surname,salary)" + "VALUES(?,?,?,?)";

		preparedStatement = connection.prepareStatement(insert);

		preparedStatement.setString(1, "Luca");
		preparedStatement.setInt(2, 43);
		preparedStatement.setString(3, "Cristian");
		preparedStatement.setDouble(4, 4000);
		preparedStatement.executeUpdate();

	}

	public static void readFromDB() throws SQLException {

		String query = "SELECT * from employee";
		PreparedStatement preparedStatement = connection.prepareStatement(query);

		ResultSet resultSet = preparedStatement.executeQuery();

		while (resultSet.next()) {
			System.out.println(resultSet.getString("name") + " " + resultSet.getInt("age") + " "
					+ resultSet.getString("surname") + " " + resultSet.getInt("salary"));

		}

	}

	public static void updateDB(String name, int age, String surname, int salary, int idemployee) {

		String query = "UPDATE employee SET name = ?, age = ?, surname = ?, salary = ? " + " where idemployee = ? ";

		try {
			PreparedStatement preparedStatement = connection.prepareStatement(query);

			preparedStatement.setString(1, name);
			preparedStatement.setInt(2, age);
			preparedStatement.setString(3, surname);
			preparedStatement.setInt(4, salary);
			preparedStatement.setInt(5, idemployee);
			preparedStatement.executeUpdate();
			preparedStatement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public static void deleteUser(int idemployee) {
		String query = "DELETE FROM employee where idemployee = ?";

		try {
			PreparedStatement preparedStatement = connection.prepareStatement(query);

			preparedStatement.setInt(1, idemployee);
			preparedStatement.execute();
			preparedStatement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

}
