package personlist;

import java.util.Scanner;
public class Person {

	public static void main(String[] args) {
		 Scanner sc = new Scanner(System.in);
	        
	        String arrOfNames[] = new String[30];
	        int arrOfAges[] = new int[30];
	        
	        int count = 0;
	       while(true) {
	           
	        System.out.print("Please tell me your name: ");
	        String name = sc.nextLine();
	          if(name.equals("EXIT")){
	            break;
	            }
	        System.out.print("Hello " + name + ", please enter you age: ");
	        int age = Integer.parseInt(sc.nextLine());
	        System.out.println("Thank you " + name + " for your registration!");
	        arrOfNames[count] = name;
	        arrOfAges[count] = age;
	        count++;
	        
	       }
	       
	       System.out.println("Below you can find a list with " + count+ " registered persons:");
	       System.out.println("Name Age");
	       
	       
	        for(int i = 0; i < count; i++) {
	            System.out.println(arrOfNames[i] + " " + arrOfAges[i] );
	            //System.out.println(arrPersons[i].name + " " + arrPersons[i].age);
	        }
	        
	      sc.close();

	}

}
