package personv3;

import java.util.Scanner;
import java.util.ArrayList;

public class Aplicatie {

	public static void main(String[] args) {	//TODO: aceasta este varianta cea mai buna
		Scanner sc = new Scanner(System.in);
		ArrayList<Person> arr = new ArrayList<Person>();
		while (true) {
			System.out.print("Please tell me your name: ");
			String name = sc.nextLine();
			if (name.equals("EXIT")) {	//TODO: as fi folosit equalsIgnoreCase
				break;
			}
			System.out.print("Hello " + name + ", please enter you age: ");
			int age = Integer.parseInt(sc.nextLine());
			System.out.println("Thank you " + name + " for your registration!");
			arr.add(new Person(name, age));
		}
		System.out.println("Below you can find a list with " + arr.size() + " registered persons:");
		System.out.println("Name Age");
		for (Person p : arr) {	//TODO: ce se intampla daca nu se inregistreaza nicio persoana si se tasteaza direct EXIT?
			System.out.println(p.name + " " + p.age);
		}
		sc.close();
	}

}
