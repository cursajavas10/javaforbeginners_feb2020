package tictactoe;

import java.util.Scanner;


public class App {
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        TicTacToe game = new TicTacToe();
        game.initializeBoard();
        System.out.println("Tic-Tac-Toe!");
        do
        {
            System.out.println("Current board layout:");
            game.printBoard();
            int row;
            int col;
            do
            {
                System.out.println("Player " + game.getCurrentPlayerMark() + ", enter an empty row and column to place your mark!");
                row = scan.nextInt();
                col = scan.nextInt();
            }
            while (!game.placeMark(row, col));
            game.changePlayer();
        }
        while(!game.checkForWin() && !game.isBoardFull());
        if (game.isBoardFull() && !game.checkForWin())	//TODO: acolada se pune imediat dupa paranteza. Ai irosit un rand
        {
            System.out.println("The game was a tie!");
        }
        else	//TODO: acolada trebuie pusa dupa else imediat
        {
            System.out.println("Current board layout:");
            game.printBoard();
            game.changePlayer();
            System.out.println(Character.toUpperCase(game.getCurrentPlayerMark()) + " Wins!");
        }
    }
}
// inspired by Google