package zoo;

public class Parrot extends Animal {

	public Parrot(String name, int age) {
		super(name, age);
	}

	public void animalKind() {
		System.out.println(name + " is a bird.");
	}
}

