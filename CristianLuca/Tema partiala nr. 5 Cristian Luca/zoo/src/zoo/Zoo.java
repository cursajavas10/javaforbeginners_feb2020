package zoo;

public class Zoo {

	Animal[] animalList;

	public Zoo(Animal[] animalList) {
		this.animalList = animalList;
	}

	void describe() {
		
		for (Animal i : animalList) {
			
			if (i != null) {
				System.out.println(i);
				i.animalKind();
				
				if (i instanceof LivesInAsia) {
					((LivesInAsia) i).livesInAsia();
				}
				
			}
			
		}
		
	}
}