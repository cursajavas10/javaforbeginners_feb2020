package zoo;
	
//TODO: de ce ai un tab pentru toata clasa? Trebuie ca definirea clasei sa inceapa fara tab.

	public abstract class Animal {
		String name;
		int age;
		
		public Animal(String name, int age) {
			this.name = name;
			this.age = age;
		}

		public String toString() {
			return "Animal [name=" + name + ", age=" + age + "]";
		}
		
		public abstract void animalKind();
	}

