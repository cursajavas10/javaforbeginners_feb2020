package zoo;

public class ZooApp {

	public static void main(String[] args) {	//TODO: este bine!!!

		Crocodile croc1 = new Crocodile("Croco", 4);
		Bear be1 = new Bear("Martin", 5);
		Lion li1 = new Lion("Fioros", 3);
		Parrot par1 = new Parrot("Coco", 7);
		Penguin pen1 = new Penguin("Ghetar", 2);

		Animal[] animalList = new Animal[100];
		animalList[0] = croc1;
		animalList[1] = be1;
		animalList[2] = li1;
		animalList[3] = par1;
		animalList[4] = pen1;

		Zoo zoo = new Zoo(animalList);
		zoo.describe();
	}
}