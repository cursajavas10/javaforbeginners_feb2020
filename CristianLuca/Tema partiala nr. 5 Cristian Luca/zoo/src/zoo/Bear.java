package zoo;


	public class Bear extends Animal {
		
		public Bear(String name, int age) {
			super(name, age);
		}

		public void animalKind() {
			System.out.println(name + " is a Mammal.");
		}
	}


