package phonebook;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class PhoneBook {

	public static void callContact(String name) {
		
		String s[] = findNumber(name);
		
		if (s != null) {
			System.out.println("Calling " + name + " at " + s[1]);
		} else {
			System.out.println("No person was found with this " + name);
		}
		
	}

	public static void saveContact(String name, long number) {
		System.out.println("Saving contact " + name + ": " + number);

		File file = new File("file.txt");

		try {
			if (!file.exists()) {
				file.createNewFile();
			}
			PrintWriter pw = new PrintWriter(new FileWriter(file, true));
			pw.println(name + ":" + number);
			pw.close();	//TODO: mereu close trebuie sa stea pe finally 

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public static String[] findNumber(String name) {
		
		try (Scanner sc = new Scanner(new File("file.txt"))) {
			String s[] = new String[0];	//TODO: pare sa fie pe modelul c++
			boolean findPerson = false;
			while (sc.hasNextLine()) {
				s = sc.nextLine().split(":");
				if (s[0].equals(name)) {
					System.out.println("The number associated with " + name + " is " + s[1]);
					findPerson = true;
					break;
				}
			}
			
			if (!findPerson) {
				System.out.println("Could not find " + name);
				s = null;
				}
			
			return s;
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return null;
		
	}

	public static void main(String[] args) {
		displayMenu();

	}

	public static void displayMenu() {
		String name;
		long number;

		try (Scanner sc = new Scanner(System.in)) {
			
		do {
			System.out.println("What would you like to do? (1, 2, 3)");
			System.out.println("1. Save contact (It's mandatory to run  this option for thefirst time!)");
			System.out.println("2. Call contact");
			System.out.println("3. Find number");
			//NU AM IDEE CUM SA FAC ACEST TASK: Remove an entry from the phone book using the phone number.
			//Am rugamintea sa-mi dai cateva sugestii legate de remove si/sau in ce alta modalitate ar trebui sa creez acest program PhoneBook
			//Iti multumesc!
			//TODO: pentru a scrie aplicatia mai usor iti sugerez sa te folosesti de colectii. De exemplu List<Person> = new ArrayList<>(); 
			//TODO: clasa Person contine atributele ce le citesti din fisier si se populeaza la citirea fiecarui rand.
			//TODO: odata ce ai lista (si nu array) o sa poti sterge foarte usor apeland methoda remove();
			int choice = sc.nextInt();
			sc.nextLine();

			switch (choice) {
			
			case 1:
				System.out.println("\nWhat is name if the person would you like to save? (FirstName LastName)");
				name = sc.nextLine();

				System.out.println("\nWhat is the phone number of the person you are saving? (e.g. 727394571)");
				number = sc.nextLong();
				sc.nextLine();

				saveContact(name, number);
				break;
				
			case 2:
				System.out.println("\nWho would you like to call? (FirstName LastName)");
				name = sc.nextLine();

				callContact(name);
				break;

			case 3:
				System.out.println("\nWhat is the person whose phone number you are searching? (FirstName LastName)");

				findNumber(sc.nextLine());
				break;

			default:
				break;
			}

			//sc.close();
			
			System.out.println("\nDo you wish to do another action? (Y/N)");
			
			if (sc.next().toLowerCase().charAt(0) != 'y') {
				break;
			}
		} while (true);
		
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

}
