package inheritance;

public class CalculatorApp {

	public static void main(String[] args) {
		Adder adder = new Adder();
		int result1 = adder.compute(2, 3);
		long maxValue = Integer.MAX_VALUE;
		long result5 = adder.compute(maxValue, 3);
		System.out.println(result1);
		System.out.println(result5);
		
		Substraction substraction = new Substraction();
		int result2 = substraction.compute(2, 3);
		System.out.println(result2);
		
		Multiply multiply = new Multiply();
		int result3 = multiply.compute(2, 3);
		System.out.println(result3);
	

	}

}
