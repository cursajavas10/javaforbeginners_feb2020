package inheritance;

public class MathsTeacher extends Teacher {
	
	void action() {
		System.out.println(name + " teach maths");
	}
}
