package inheritance;

public class SchoolApp {

	public static void main(String[] args) {
		Teacher t = new Teacher();
		t.name = "Vlad";
		t.action();
		
		MathsTeacher m = new MathsTeacher();
		m.name = "Adrian";
		m.action();
		
		PhysicsTeacher p = new PhysicsTeacher();
		p.name = "Luminita";
		p.action();
	}

}
