package inheritance;

public class Adder {
	
	long compute(long a, int b) {
		return a+b;
	}
	
	int compute(int a, int b) {
		return a+b;
	}
	
	int compute(int a, int b, int c) {
		return a+b+c;
	}
	
	int compute(int a, int b, int c, int d) {
		return a+b+c+d;
	}
}
