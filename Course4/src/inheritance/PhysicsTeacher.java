package inheritance;

public class PhysicsTeacher extends Teacher {
	
	void action() {
		System.out.println(name + " teach physics");
	}
}
