package statics;

public class Cube {
	int volume;
	String color;
	private int area;
	
	public Cube(int volume, String color) {
		super();
		this.volume = volume;
		this.color = color;
		area = volume/2;
	}

	@Override
	public String toString() {
		return "Cube [volume=" + volume + ", color=" + color + ", area=" + area + "]" ;
	}
	
	
}
