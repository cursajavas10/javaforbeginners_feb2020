package statics;

public class Circle {
	int area;
	String color;
	int length;
	
	public Circle(int area, String color, int length) {
		super();
		this.area = area;
		this.color = color;
		this.length = length;
	}

	@Override
	public String toString() {
		return "Circle [area=" + area + ", color=" + color + ", length=" + length + "]";
	}
	
	
	
}
