package statics;

public class Square {
	int area;
	String color;
	int perimeter;
	
	public Square(int area, String color, int perimeter) {
		super();
		this.area = area;
		this.color = color;
		this.perimeter = perimeter;
	}

	@Override
	public String toString() {
		return "Square [area=" + area + ", color=" + color + ", perimeter=" + perimeter + "]";
	}
	
	
	
}
