package statics;

public class ShapeApp {
	static String COLOR_RED = "RED";
	static String COLOR_YELLOW = "YELLOW";
	static String COLOR_BLUE = "BLUE";

	public static void main(String[] args) {
		Circle[] list = new Circle[4];
		Circle c1 = new Circle(10, ShapeApp.COLOR_RED, 55);
		Circle c2 = new Circle(15, ShapeApp.COLOR_YELLOW, 65);
		Circle c3 = new Circle(17, ShapeApp.COLOR_BLUE, 45);
		Circle c4 = new Circle(13,  ShapeApp.COLOR_RED, 85);
		list[0]=c1;
		list[1]=c2;
		list[2]=c3;
		list[3]=c4;
		
		Cube cube = new Cube(44,  ShapeApp.COLOR_RED);
		
		Square s1 = new Square(44,  ShapeApp.COLOR_RED, 88);
		Square s2 = new Square(47, ShapeApp.COLOR_YELLOW, 90);
		
		for(int i=0; i<list.length; i++) {
			if(list[i].color.equals(ShapeApp.COLOR_RED)) {
				System.out.println(list[i]);
			}
		}
		System.out.println(cube);
		System.out.println(s1);
		System.out.println(s2);

	}

}
