package tema2;

import java.util.Scanner;

public class Tema12 {
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Please insert a number:");
		int n = sc.nextInt();
		
		int S=0;
		int c=1;
		int i=0;
		
		System.out.print("The odd numbers are: ");
		
		while(c<=n) {
			if(i%2==1) {
				System.out.print(i+", ");
				S=S+i;
				c++;
			}
			i++;
		}
		
		System.out.println(" ");
		System.out.println("The Sum of odd Natural Number up to "+n+" terms is: "+S);
		
		sc.close();
	}
}
