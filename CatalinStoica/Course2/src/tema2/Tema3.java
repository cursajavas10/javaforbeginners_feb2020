package tema2;

import java.util.Scanner;

public class Tema3 {
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Please enter a character:");
		char ch = sc.next().charAt(0); //de intrebat faza cu indexul
		
		int ascii = ch;
		System.out.println("The value of the character '"+ch+"' is: "+ascii);
		
		sc.close();
		
	}
}
