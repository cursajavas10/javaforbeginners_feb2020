package tema2;

import java.util.Scanner;

public class Tema5 {
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Please insert an 'integer' type value:");
		int i = sc.nextInt();
		
		System.out.println("Please insert a 'double' type value:");
		double d = sc.nextDouble();
		
		String i2 = Integer.toHexString(i);
		String d2 = Double.toHexString(d);
		
		System.out.println("The Hex value of the integer '"+i+"' is: "+i2);
		System.out.println("The Hex value of the double '"+d+"' is: "+d2);
		
		sc.close();
	}
}
