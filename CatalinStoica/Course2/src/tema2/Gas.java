package tema2;

import java.util.Scanner;

public class Gas {
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Please enter Start KM:");
		float start = sc.nextFloat();
		
		System.out.println("Please enter End KM:");
		float end = sc.nextFloat();
		
		System.out.println("Please enter consumption in city:");
		float cityCons = sc.nextFloat();
		
		System.out.println("Please enter consumption on the highway:");
		float highwayCons = sc.nextFloat();
		
		System.out.println("Where will be the trip? True for City, False for Highway");
		boolean trip = sc.nextBoolean();
		
		calcul(start, end, cityCons, highwayCons, trip);
		
		sc.close();
	}

	private static void calcul(float start, float end, float cityCons, float highwayCons, boolean trip) {
		float cons=0;
		
		if(trip == true) {
			cons=((end-start)*cityCons)/100;
		} else cons=((end-start)*highwayCons)/100;
		
		System.out.println("The amount of liters consumed will be: "+Math.abs(cons));
	}
}
