package tema2;

import java.util.Scanner;

public class Tema9 {
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
	
		System.out.println("Please insert 1st number:");
		int n1 = sc.nextInt();
		
		System.out.println("Please insert 2nd number:");
		int n2 = sc.nextInt();
		
		System.out.println("Please insert 3rd number:");
		int n3 = sc.nextInt();
		
		System.out.println("Please insert 4th number:");
		int n4 = sc.nextInt();
		
		System.out.println("Please insert 5th number:");
		int n5 = sc.nextInt();
		
		System.out.println("The numbers are: "+n1+", "+n2+", "+n3+", "+n4+", "+n5);
		
		int S = n1+n2+n3+n4+n5;
		System.out.println("The sum of the numbers is: "+S);
		
		float A = (n1+n2+n3+n4+n5)/5;
		System.out.println("The average is: "+A);
		
		sc.close();
	}
}
