package tema2;

import java.util.Scanner;

public class Tema10 {
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Please insert a number:");
		int n = sc.nextInt();
		
		if(n>0) {
			System.out.println("Number is positive");
		} else {
			System.out.println("Number is negative");
		}
		
		sc.close();
	}
}
