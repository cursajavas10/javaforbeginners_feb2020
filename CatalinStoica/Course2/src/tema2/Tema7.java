package tema2;

import java.util.Scanner;

public class Tema7 {
	public static void main (String arg[]) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Please insert a letter:");
		String s = sc.nextLine();
		
		if(s.length() > 1) {
			System.out.println("Error: You should insert only one character");
		} else if(s.length() == 1) {
			char c = s.charAt(0);
			if(c >= 'A' && c <='z') {
				String mesaj = c == 'a'  || c == 'e' || c == 'i' || c == 'o' || c == 'u' || c == 'A' || c == 'E' || c == 'I' || c == 'O' || c == 'U' ? "Vowel":"Consonant";
				System.out.println(mesaj);
			} else System.out.println("Error: You should insert an alphabetical letter");
		}
		
		sc.close();
	}
}
