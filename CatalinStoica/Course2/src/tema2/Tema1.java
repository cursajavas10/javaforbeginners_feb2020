package tema2;

import java.util.Scanner;

public class Tema1 {
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Please enter base-width:");
		double b = sc.nextDouble();
		
		System.out.println("Please enter height:");
		double h = sc.nextDouble();
		
		double A = (b*h)/2;
		
		System.out.println("The area of the triangle is:"+A);
		
		sc.close();
	}
}
