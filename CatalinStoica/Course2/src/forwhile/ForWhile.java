package forwhile;

public class ForWhile {
	public static void main(String[] args) {
		
		
		//FOR
		/*for(int i=0;i<=10;i++) {
			if(i%2!=0) {
				System.out.println(i);
			}
		}*/
		
		//WHILE DO
		/*int i=1;
		while(i<=10) {
			System.out.println(i);
			i+=2;
		}*/
		
		//DO WHILE
		/*int i=1;
		do {
			System.out.println(i);
			i+=2;
		} while(i<=10);*/
		
		//Numere divizibile cu 10
		/*for(int i=100;i>15;i-=10) {
			//if(i%10==0) 
				System.out.println(i);
			
		}*/
		
		//Nr nedivizibile cu 3
		for(int i=0;i<=10;i++) {
			if(i%3!=0) {
				System.out.println(i);
			}
		}
		
	}
}
