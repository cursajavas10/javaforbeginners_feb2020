package operators;

import java.util.Scanner; // librarie pt scanner

public class OperatorsApp {
	public static void main (String args[]) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Please insert the age:");
		int age = sc.nextInt();
		sc.reset(); // trebuie inchis ca sa nu ne mai dea warning
		
		//aici pentru ca nextInt are un bug, redeclaram scannerul
		
		System.out.println("Please insert income:");
		double income = sc.nextDouble();
		
		System.out.println("Please insert gender:");
		boolean gender = sc.nextBoolean();
		
		
		/*String mesaj1 = age > 20 && age < 35 ? "Yes, your age is between 20 and 35":"No, your age is not between 20 and 35";
		System.out.println(mesaj1);*/
		

		if(age > 20 && age < 35) {
			System.out.println("Yes");
		} else {
			System.out.println("No");
		}
		
		boolean result = (gender==true && income>4000) || (gender==false && income>5000);
		System.out.println(result);
		
		
		
		sc.close(); // trebuie inchis ca sa nu ne mai dea warning
	}
}
