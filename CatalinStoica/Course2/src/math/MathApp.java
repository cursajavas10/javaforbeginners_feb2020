package math;

public class MathApp {
	public static void main (String args[]) {
		
		//int result = 2 + 2*2 + 2*2*2 + 2*2*2*2 + 2*2*2*2*2;
		double result = Math.pow(2,1) + Math.pow(2,2) + Math.pow(2,3) + Math.pow(2,4) + Math.pow(2,5);
		System.out.println(result);
		
		double result2 = Math.abs(-256*3/5+1);
		System.out.println(result2);
		
		double result3 = Math.round(70.335/(2+1));
		System.out.println(result3);
		
		float a=3.4f, b=3.7f;
		System.out.println(a+b);
	}
}
