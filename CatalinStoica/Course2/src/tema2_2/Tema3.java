package tema2_2;

import java.util.Scanner;

public class Tema3 {
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Please insert a number:");
		int n = sc.nextInt();
		
		int S = 0;
		
		while(n>0) {
			
			S = S+(n%10);
			n = n/10;
			
		}
		
		System.out.println("Sum of digits: "+S); 
		
		sc.close();
	}
}
