package tema2_2;

public class Tema8 {
	public static void main(String[] args) {
		
		double a = Math.sqrt(81);
		System.out.println(a);
		double b = Math.sqrt(((5/2+3)-6*8/3)/2+3);
		System.out.println(b);
		
		//The result is "NaN" for b because it computes sqrt of a negative number which is arithmetically undefined.
	}
}
