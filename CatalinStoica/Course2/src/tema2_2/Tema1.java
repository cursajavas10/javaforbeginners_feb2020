package tema2_2;

import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class Tema1 {
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Please insert number of years:");
		long y = sc.nextLong();
		
		System.out.println("Total amount of seconds is: "+TimeUnit.DAYS.toSeconds(y*365));
		
		sc.close();
	}
}
