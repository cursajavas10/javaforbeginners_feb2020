package tema2_2;

public class Tema6 {
	
	public static void main(String[] args) {
		
		System.out.println(String.format("Result of 11223344 + 0.3 with 10 decimals is: %.10f", 11223344+0.3));
		System.out.println(String.format("Result of 1111+1111.22 without decimals is: %.0f", 1111+1111.22));
		System.out.println(String.format("Result of 1+0.1234 with 2 decimals is: %.2f", 1+0.1234));
	}
}
