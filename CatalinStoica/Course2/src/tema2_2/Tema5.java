package tema2_2;

import java.util.Scanner;

public class Tema5 {
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Please insert the length of a side in cm:");
		double side = sc.nextDouble();
		
		double A = (3*Math.sqrt(3)*Math.pow(side, 2))/2;
		
		System.out.print("The area of the hexagon is: ");
		System.out.format("%.2f", A);
		System.out.print(" cm^2");
				
		sc.close();
	}
}
