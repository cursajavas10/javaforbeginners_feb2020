package week;

import java.util.Scanner;

public class WeekApp {
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int n = sc.nextInt();
		
		switch(n) {
		case 1: System.out.println("Monday");break;
		case 2: System.out.println("Tuesday");break;
		case 3: System.out.println("Wednesday");break;
		case 4: System.out.println("Thursday");break;
		case 5: System.out.println("Firday");break;
		case 6: System.out.println("Saturday");break;
		case 7: System.out.println("Sunday");break;
		default: System.out.println("Numar gresit");
		}
		
		if(n>=0 && n<=3) {
			System.out.println("[0-3]");
		} else if (n>=4 && n<=5) {
			System.out.println("[4-5]");
		} else if (n>=6 && n<=7) {
			System.out.println("[6-7]");
		} else if (n==8) {
			System.out.println("[8-8]");
		} else {
			System.out.println("other");
		}
		
		//varianta 1
		switch(n) {
		case 0:System.out.println("[0-3]");break;
		case 1:System.out.println("[0-3]");break;
		case 2:System.out.println("[0-3]");break;
		case 3:System.out.println("[0-3]");break;
		case 4:System.out.println("[4-5]");break;
		case 5:System.out.println("[4-5]");break;
		case 6:System.out.println("[6-7]");break;
		case 7:System.out.println("[6-7]");break;
		case 8:System.out.println("[8-8]");break;
		default:System.out.println("other");
		}
		
		//varianta 2
		switch(n) {
		case 0:
		case 1:
		case 2:
		case 3:System.out.println("[0-3]");break;
		case 4:
		case 5:System.out.println("[4-5]");break;
		case 6:
		case 7:System.out.println("[6-7]");break;
		case 8:System.out.println("[8-8]");break;
		default:System.out.println("other");
		}
		
		sc.close();
	}
}
