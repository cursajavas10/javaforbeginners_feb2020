package game;

import java.util.Scanner;

public class GameApp {
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Please insert your name:");
		String firstName = sc.nextLine();
		
		System.out.println("Please insert your number:");
		int firstNumber = sc.nextInt();
		
		sc.nextLine(); // ca sa dispara warning, sau ca sa mearga, sau sc.reset();
		
		System.out.println("Please insert your name:");
		String secondName = sc.nextLine();
		
		System.out.println("Please insert your number:");
		int secondNumber = sc.nextInt();
		
		/*if (firstNumber > secondNumber) {
			System.out.println(firstName);
		} else if(secondNumber > firstNumber) {
			System.out.println(secondName);
		} else {
			System.out.println("Egalitate");
		}*/
		
		System.out.println(firstNumber > secondNumber ?
				firstName : (secondNumber>firstNumber ? secondName : "egalitate"));
		sc.close();
	}
}
