
public class Tema5 {
	
	public static void main(String args[]) {	
		
		Country5 France;
		France = new Country5();
		
		France.name="France";
		France.continent="Europe";
		
		City5 Paris;
		Paris = new City5();
		
		Paris.name="Paris";
		Paris.noOfPeople=2148271;
		Paris.district="Iles-de-France";
		Paris.country=France.name;
		Paris.continent=France.continent;
		
		Paris.describe();	
	}	
}
