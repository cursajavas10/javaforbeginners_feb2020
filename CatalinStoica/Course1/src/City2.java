
public class City2 {
	
	public static void main (String args[]) {
		class Town {
			String name;
			String country;
			int noOfPeople;
		
			void describe() {
				System.out.println(name+" is the capital of "+country+" and has a population of: "+noOfPeople);
			}
		}
		
		Town Madrid;
		Madrid = new Town();
		Madrid.name="Madrid";
		Madrid.country="Spain";
		Madrid.noOfPeople=3223334;
		
		Madrid.describe();
	}
}
