
public class City5 {
	
	String name;
	int noOfPeople;
	String district;
	String country;
	String continent;
			
	void describe() {
		System.out.println(name+" is the capital of "+country+", has a population of "+noOfPeople+" people, it is part of the "+district+" district and it is located on the continent of "+continent+".");
	}
}
		
	