package loops;

public class LoopsApp {

	public static void main(String[] args) {
		for(int i=1; i<=10; i=i+2) {
			System.out.println(i);
		}
		
		for(int i=0; i<=10; i=i+1) {
			if(i%2 != 0) {
				System.out.println(i);
			}
		}
		
		int i = 1;
		while(i<=10) {
			System.out.println(i);
			i+=2;
		}
		
		int j = 1;
		do {
			System.out.println(j);
			j+=2;
		} while (j <= 10);
		
		for(int k=100; k>15; k = k -10){
			System.out.println(k);
		}
		
		for(i=0; i<=10; i++) {
			if (i%3 != 0) {
				System.out.println(i);
			}
		}

	}

}


