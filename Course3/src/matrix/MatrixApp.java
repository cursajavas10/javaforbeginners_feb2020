package matrix;

import java.util.Scanner;

public class MatrixApp {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		System.out.println("Insert no of rows and cols:");
		int noRows = Integer.parseInt(sc.nextLine());
		int noCols = Integer.parseInt(sc.nextLine());
		
		int[][] matrix = new int[noRows][noCols];
		
		System.out.println("Insert matrix values:");
		for(int i=0; i<noRows; i++) {
			for(int j=0; j<noCols; j++) {
				matrix[i][j] = Integer.parseInt(sc.nextLine());
			}
		}
		
		for(int i=0; i<noRows; i++) {
			for(int j=0; j<noCols; j++) {
				System.out.print(matrix[i][j] + " ");
			}
			System.out.println();
		}
		

	}

}
