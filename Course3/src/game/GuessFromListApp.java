package game;

import java.util.Scanner;

public class GuessFromListApp {

	public static void main(String[] args) {
		int secretNumber = 10;
		int noOfTries = 3;
		int[] list = new int[noOfTries];
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Please insert your name:");
		String name = sc.nextLine();
		
		for(int i=0; i<noOfTries; i++) {
			System.out.println("Please tell me your number:");
			int number = Integer.parseInt(sc.nextLine());
			
			if(number == secretNumber) {
				System.out.println("You WON!");
				break;	//daca a castigat iesim din bucla
			} else {
				list[i] = number;
				if(i == noOfTries - 1) {	//daca este ultima incercare afisam GAME OVER!
					System.out.println("GAME OVER!");
					for(int j=0; j<list.length; j++) {	//iteram pe array pentru a afisa toate valorile
						System.out.print(list[j] + " ");
					}
				} else {
					System.out.println(name + ", please try again!!!");
				}
			}
		}
		
		

	}

}
