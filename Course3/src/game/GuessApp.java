package game;

import java.util.Scanner;

public class GuessApp {

	public static void main(String[] args) {
		//int secretNumber = 10;
		int[] secretNumbers = {10, 11, 12, 13};
		int noOfTries = 3;
		int[] list = new int[noOfTries];
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Please insert your name:");
		String name = sc.nextLine();
		
		loop1:
		for(int i=0; i<noOfTries; i++) {
			System.out.println("Please tell me your number:");
			int number = Integer.parseInt(sc.nextLine());
			
			boolean isWinner = false;
			for(int k=0; k<secretNumbers.length; k++) {	//iteram pe array-ul de numere castigatoare pentru a vedea daca am ghicit
				if(number == secretNumbers[k]) {
					System.out.println("You WON!");
					isWinner = true;
					break loop1;	//daca a castigat iesim din bucla
				} else {
					list[i] = number;

				}
			}
			if(isWinner == false) {
				if(i == noOfTries - 1) {	//daca este ultima incercare afisam GAME OVER!
					System.out.println("GAME OVER!");
					for(int j=0; j<list.length; j++) {	//iteram pe array pentru a afisa toate valorile
						System.out.print(list[j] + " ");
					}
				} else {
					System.out.println(name + ", please try again!!!");
				}
			}
		}
		
		

	}

}
