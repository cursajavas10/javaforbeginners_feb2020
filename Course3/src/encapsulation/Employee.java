package encapsulation;

public class Employee {
	String name;
	String surname;
	double salary;
	int age;
	
	void setName(String n) {
		//pot scrie mult cod aici!!!!
		name = n;
		if(name.equals("Vlad")) {
			System.out.println("WARNING!! Contact police!!");
		}
	}
	
	String getName() {
		//pot scrie mult cod aici!!!!
		return name;
	}

	public String toString() {
		return "Employee [name=" + name + ", surname=" + surname + ", salary=" + salary + ", age=" + age + "]";
	}
	
	
}
