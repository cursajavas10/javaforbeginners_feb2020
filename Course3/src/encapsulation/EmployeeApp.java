package encapsulation;

public class EmployeeApp {

	public static void main(String[] args) {
		Employee e1 = new Employee();
		e1.setName("Vlad");
		e1.salary = 5000;
		
		System.out.println(e1);
		System.out.println(e1.name);
	}

}
