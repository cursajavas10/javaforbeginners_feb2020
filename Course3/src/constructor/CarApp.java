package constructor;

public class CarApp {

	public static void main(String[] args) {
		Engine e1 = new Engine();
		e1.name = "V8";
		e1.power = 116;
		
		Engine e2 = new Engine();
		e2.name = "V16";
		e2.power = 150;
		
		Engine e3 = new Engine();
		e3.power = 180;
		
		Car c1 = new Car(4, 1, 130, e3);
		Car c2 = new Car(4, 1, 110, e2);
		Car c3 = new Car(4, 1, 130, e2);
		Car c4 = new Car(2, 1, 110, e1);
		
		System.out.println(c1);
		System.out.println(c2);
		System.out.println(c3);
		System.out.println(c4);
		
	}

}
