package constructor;

public class Car {
	int noOfWheels;
	int noOfDrivers;
	int speed;
	Engine engine;
	

	public Car(int noOfWheels, int noOfDrivers, int speed, Engine engine) {
		this.noOfWheels = noOfWheels;
		this.noOfDrivers = noOfDrivers;
		this.speed = speed;
		this.engine = engine;
	}


	public String toString() {
		return "Car [noOfWheels=" + noOfWheels + ", noOfDrivers=" + noOfDrivers + ", speed=" + speed + ", engine="
				+ engine + "]";
	}
	
	
}
