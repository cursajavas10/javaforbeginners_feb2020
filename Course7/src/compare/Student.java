package compare;

import java.util.Comparator;

public class Student implements Comparable<Student> {
	String name;
	int age;
	String university;
	boolean gender;
	
	public Student(String name, int age, String university, boolean gender) {
		super();
		this.name = name;
		this.age = age;
		this.university = university;
		this.gender = gender;
	}

	@Override
	public String toString() {
		return "Student [name=" + name + ", age=" + age + ", university=" + university + ", gender=" + gender + "]";
	}

	@Override
	public int compareTo(Student o) {
		return this.age - o.age;
	}

	
	
}
