package compare;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class StudentsApp {

	public static void main(String[] args) {
		Student s1 = new Student("Adi", 30, "Poli", true);
		Student s2 = new Student("Alex", 37, "Poli", true);
		Student s3 = new Student("Marius", 25, "ASE", true);
		Student s4 = new Student("Octavian", 37, "Poli", true);
		
		List<Student> list = new ArrayList<>();
		list.add(s1);
		list.add(s2);
		list.add(s3);
		list.add(s4);
		
		System.out.println(list);
		
		Collections.sort(list, Collections.reverseOrder());
		
		System.out.println(list);
		
		Collections.sort(list, new NameComparator());
		
		System.out.println(list);
	}

}
