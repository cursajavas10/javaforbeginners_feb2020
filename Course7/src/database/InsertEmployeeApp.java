package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class InsertEmployeeApp {

	public static void main(String[] args) throws Exception {
		Class.forName("com.mysql.jdbc.Driver"); 
		Connection connection = DriverManager
			    .getConnection("jdbc:mysql://localhost:3306/dbemployee", "root", "root");
		Statement stmt = connection.createStatement();
		
		String name = "Andreea";
		
		stmt.executeUpdate("INSERT INTO `dbemployee`.`employee` (`name`, `age`, `surname`, `salary`) VALUES ('" + name + "', '34', 'Adam', '6000');");
		
		connection.close();
		
	}

}
