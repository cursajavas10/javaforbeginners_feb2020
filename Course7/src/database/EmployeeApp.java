package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class EmployeeApp {

	public static void main(String[] args) throws Exception {
		Class.forName("com.mysql.jdbc.Driver"); 
		Connection connection = DriverManager
			    .getConnection("jdbc:mysql://localhost:3306/dbemployee", "root", "root");
		Statement stmt = connection.createStatement();
		ResultSet rs = stmt.executeQuery("select * from employee;");
		
		while(rs.next()) {
			System.out.println(rs.getInt(1) + " " + rs.getString("name") + " " + rs.getDouble("salary"));
		}
		
		connection.close();
		
	}

}
