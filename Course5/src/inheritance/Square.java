package inheritance;

public class Square extends Shape {

	int perimeter;

	public Square(int perimeter, int length, int area) {
		super(length, area);
		this.perimeter = perimeter;
	}

	@Override
	public String toString() {
		return "Square [perimeter=" + perimeter + ", length=" + length + ", area=" + area + "]";
	}
	
	
	
}
