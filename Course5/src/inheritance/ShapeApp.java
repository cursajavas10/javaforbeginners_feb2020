package inheritance;

public class ShapeApp {

	public static void main(String[] args) {
		Rectangle r1 = new Rectangle(1,3,5,6);
		Circle c1 = new Circle(4,7);
		Circle c2 = new Circle(24,87);
		Circle c3 = new Circle(44,97);
		Square s1 = new Square(4,60,230);
		Square s2 = new Square(40000,6000,230000);
		
		/*Rectangle[] rList = new Rectangle[100];
		rList[0] = r1;
		
		Circle[] cList = new Circle[100];
		cList[0] = c1;
		cList[1] = c2;
		cList[2] = c3;*/
		
		/*Shape shape;
		shape = new Circle(4, 7);
		if(2==2) {
			shape = new Square(3,4,5);
		}*/
		
		Shape[] sList = new Shape[100];
		sList[0] = r1;
		sList[1] = c1;
		sList[2] = c2;
		sList[3] = c3;
		sList[4] = s1;
		sList[5] = s2;
		
		
		Game game = new Game(sList);
		game.describe();

	}

}
