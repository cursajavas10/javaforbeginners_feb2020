package inheritance;

public class Game {
	Shape[] sList;
	
	public Game(Shape[] sList) {
		this.sList = sList;
	}
	
	void describe() {
		for(int i=0; i<sList.length; i++) {
			if(sList[i] != null) {
				//System.out.println(sList[i]);
				System.out.println("Shape:" + sList[i].area + " " + sList[i].length);
				if(sList[i] instanceof Square) {
					Square s = (Square) sList[i];
					System.out.println("    Square:" + s.perimeter);
				}
				System.out.println();
			}
		}
	}

}
