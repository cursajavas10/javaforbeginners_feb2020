package inheritance;

public class Rectangle extends Square {
	int width;
	
	public Rectangle(int width, int perimeter, int length, int area) {
		super(perimeter, length, area);
		this.width = width;
	}

	@Override
	public String toString() {
		return "Rectangle [width=" + width + ", perimeter=" + perimeter + ", length=" + length + ", area=" + area + "]";
	}
	
	
}
