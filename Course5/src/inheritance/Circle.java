package inheritance;

public class Circle extends Shape {

	public Circle(int length, int area) {
		super(length, area);
	}

	@Override
	public String toString() {
		return "Circle [length=" + length + ", area=" + area + "]";
	}
	
	
}
