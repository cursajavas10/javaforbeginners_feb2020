package animals;

public class Zebra extends Animal {

	public Zebra(String name) {
		super(name);
	}

	@Override
	public void eat() {
		System.out.println(name + " eat grass");
	}

}
