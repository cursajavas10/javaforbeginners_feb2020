package animals;

public class Penguin extends Animal {
	
	
	public Penguin(String name) {
		super(name);
	}
	
	public void eat() {
		System.out.println(name + " eat fish");
	}
}
