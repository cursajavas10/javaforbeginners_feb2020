package animals;

public class Giraffe extends Animal {

	public Giraffe(String name) {
		super(name);
	}
	
	public void eat() {
		System.out.println(name + " eat grass");
	}
}
