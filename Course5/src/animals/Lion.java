package animals;

public class Lion extends Animal implements IsTheKing {

	
	public Lion(String name) {
		super(name);
	}

	public void eat() {
		System.out.println(name + " eat meat");
	}

	@Override
	public void isSleeping() {
		System.out.println("ZzzZZZ");
	}
}
