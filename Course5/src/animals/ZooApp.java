package animals;

public class ZooApp {

	public static void main(String[] args) {
		Giraffe g = new Giraffe("GiraffeOne");
		Penguin p1 = new Penguin("Pop1");
		Penguin p2 = new Penguin("Pop2");
		Lion l = new Lion("Zumba");
		
		Animal[] list = new Animal[100];
		list[0] = g;
		list[1] = p1;
		list[2] = p2;
		list[3] = l;
		
		Zoo zoo = new Zoo(list);
		zoo.describe();

	}

}
