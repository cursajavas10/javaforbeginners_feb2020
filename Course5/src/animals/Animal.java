package animals;

public abstract class Animal {
	String name;

	public Animal(String name) {
		super();
		this.name = name;
	}

	@Override
	public String toString() {
		return "Animal [name=" + name + "]";
	}
	
	public abstract void eat();
	
	
}
