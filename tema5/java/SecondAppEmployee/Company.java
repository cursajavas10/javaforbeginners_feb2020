package tema5.java.SecondAppEmployee;

import java.awt.List;
import java.io.LineNumberInputStream;
import java.util.ArrayList;

public class Company {
	public ArrayList<Employee> lsitaE = new ArrayList<>();

	public Company(ArrayList<Employee> lsitaE) {
		super();
		this.lsitaE = lsitaE;
	}

	@Override
	public String toString() {
		return "Company [lsitaE=" + lsitaE + "]";
	};

	void descriereAngajati() {
		for (Employee e : lsitaE) {
			if (e instanceof Programmers) {
				Programmers programmers=(Programmers) e;
				if (programmers.isManager == true) {
					System.out.println(programmers.toString());
					System.out.println(programmers.name + " " + " este managerul departamentului de IT");

				} else if (programmers.isTehnicalLead == true) {
					System.out.println(programmers.toString());
					System.out.println(programmers.name + " " + " este tehnical lead");
				} else {
					System.out.println(programmers.toString());
				}
			}else if(e instanceof DbProgrammers) {
				DbProgrammers dbProgrammers=(DbProgrammers) e;
				if (dbProgrammers.isManager == true) {
					System.out.println(dbProgrammers.toString());
					System.out.println(dbProgrammers.name + " " + " este managerul departamentului de IT");

				} else if (dbProgrammers.isTehnicalLead == true) {
					System.out.println(dbProgrammers.toString());
					System.out.println(dbProgrammers.name + " " + " este tehnical lead");
				} else {
					System.out.println(dbProgrammers.toString());
				}
			}else if(e instanceof Analyst) {
				Analyst analyst=(Analyst) e;
				if (analyst.isManager == true) {
					System.out.println(analyst.toString());
					System.out.println(analyst.name + " " + " este managerul departamentului de IT");

				} else if (analyst.isTehnicalLead == true) {
					System.out.println(analyst.toString());
					System.out.println(analyst.name + " " + " este tehnical lead");
				} else {
					System.out.println(analyst.toString());
				}
			}

		}

	}

	Employee cautareManager(ArrayList<Employee> listaEmployee) {
		Employee eCautat = new Employee(null, null, 0, 0, false, false);
		for (Employee e : listaEmployee) {
			if (e.isManager == true) {
				eCautat = e;
			}
		}
		return eCautat;

	}

	void setManagerIT(String nume, String surname, Employee eM) {
		for (Employee e : this.lsitaE) {
			if (e.name.matches(nume) && e.surname.matches(surname)) {
				eM.isManager = false;
				e.isManager = true;
				break;
			}

		}
		System.out.println("Scimbarea a fost efectuata cu succes!");

	}

}
