package tema5.java.SecondAppEmployee;

public class DbProgrammers extends Employee {
	
	public DbProgrammers(String name, String surname, int age, double salary, boolean isManager,
			boolean isTehnicalLead,double bonus) {
		super(name, surname, age, salary, isManager, isTehnicalLead);
		this.bonus=bonus;
	}

	public double bonus;

	@Override
	public String toString() {
		return "DbProgrammers [bonus=" + bonus + ", name=" + name + ", surname=" + surname + ", age=" + age
				+ ", salary=" + salary + ", isManager=" + isManager + ", isTehnicalLead=" + isTehnicalLead + "]";
	}
	
	


	
	

}
