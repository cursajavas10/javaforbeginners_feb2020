package tema5.java.SecondAppEmployee;

public class Employee {
	public String name;
	public String surname;
	public int age;
	public double salary;
	public boolean isManager;
	public boolean isTehnicalLead;
	
	public Employee(String name, String surname, int age, double salary, boolean isManager, boolean isTehnicalLead) {
		super();
		this.name = name;
		this.surname = surname;
		this.age = age;
		this.salary = salary;
		this.isManager = isManager;
		this.isTehnicalLead = isTehnicalLead;
	}

	@Override
	public String toString() {
		return "Employee [name=" + name + ", surname=" + surname + ", age=" + age + ", salary=" + salary
				+ ", isManager=" + isManager + ", isTehnicalLead=" + isTehnicalLead + "]";
	}
	

	
	
	
	
	

}
