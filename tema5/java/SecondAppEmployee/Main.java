package tema5.java.SecondAppEmployee;

import java.util.ArrayList;
import java.util.Scanner;

import CURS.S4.Mostenire.Calculator;

public class Main {
public static ArrayList<Employee> lista=new ArrayList<>();
	public static void main(String[] args) {
		DbProgrammers dbProgrammers1=new DbProgrammers("Andrei", "Alexandru", 24, 1000, false, false, 1500);
		DbProgrammers dbProgrammers2=new DbProgrammers("Taras", "Dorin", 26, 1000, false, false, 1600);
		DbProgrammers dbProgrammers3=new DbProgrammers("Andrei", "Marin", 29, 2800,false, true, 1900);
		
		Analyst analyst1=new Analyst("Alexandra", "Stefania", 22, 1000, false, false);
		Analyst analyst2=new Analyst("Margarita", "Andreea", 24, 1500, false, false);
		Analyst analyst3=new Analyst("Alexandru", "Constantin", 28, 2000, false, true);
		
		Programmers programmers1=new Programmers("Taras", "Dorin", 20, 1300, false, false, 1000);
		Programmers programmers2=new Programmers("Adrian", "Balan", 24, 2000, false, true, 1500);
		Programmers programmer3=new Programmers("Vlad", "Vlad", 30, 3000, true, false, 1800);
		Programmers programmers4=new Programmers("Ion", "Vasile", 27, 2500,false, false, 13500);
		
		lista.add(analyst1);
		lista.add(analyst2);
		lista.add(analyst3);
		lista.add(dbProgrammers1);
		lista.add(dbProgrammers2);
		lista.add(dbProgrammers3);
		lista.add(programmers1);
		lista.add(programmers2);
		lista.add(programmer3);
		lista.add(programmers4);
		
		
		Company company=new Company(lista);
		company.descriereAngajati();
		/*
		System.out.println("------------------------------------------");
		System.out.println("Enter the name of the new Manager:");
		Scanner sc=new Scanner(System.in);
		String nume=sc.next();
		System.out.println("Enter the surname of the new Manager:");
		Scanner sc1=new Scanner(System.in);
		String sourname=sc1.nextLine();
		company.setManagerIT(nume,sourname, company.cautareManager(lista));
		System.out.println("-------------------------------");
		company.descriereAngajati();
		*/
		

	}
}
