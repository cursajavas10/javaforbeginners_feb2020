package tema5.java.SecondAppEmployee;

public class Analyst extends Employee {

	public Analyst(String name, String surname, int age, double salary, boolean isManager, boolean isTehnicalLead) {
		super(name, surname, age, salary, isManager, isTehnicalLead);
		
	}

	@Override
	public String toString() {
		return "Analyst [name=" + name + ", surname=" + surname + ", age=" + age + ", salary=" + salary + ", isManager="
				+ isManager + ", isTehnicalLead=" + isTehnicalLead + "]";
	}
	
	
	

}
