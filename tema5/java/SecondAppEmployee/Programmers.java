package tema5.java.SecondAppEmployee;

public class Programmers extends Employee {
	public double bonus;

	public Programmers(String name, String surname, int age, double salary, boolean isManager, boolean isTehnicalLead,
			double bonus) {
		super(name, surname, age, salary, isManager, isTehnicalLead);
		this.bonus = bonus;
	}

	@Override
	public String toString() {
		return "Programmers [bonus=" + bonus + ", name=" + name + ", surname=" + surname + ", age=" + age + ", salary="
				+ salary + ", isManager=" + isManager + ", isTehnicalLead=" + isTehnicalLead + "]";
	}

	
	
	

	
	
	

}
