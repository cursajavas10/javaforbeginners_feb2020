package tema5.java.FirstApp;

import java.util.ArrayList;

public class Zoo {
	public ArrayList<Animal> listaAnimale=new ArrayList<>();

	public Zoo(ArrayList<Animal> listaAnimale) {
		super();
		this.listaAnimale = listaAnimale;
	}
	
	void descriereAnimale() {
		for(Animal a : listaAnimale) {
			if(a instanceof Mammals) {
				Mammals mammals=(Mammals) a;
				mammals.describeMammals();
			}
			if(a instanceof Birds) {
				Birds birds=(Birds) a;
				birds.describeBirds();
			}
			if(a instanceof Reptiles) {
				Reptiles reptiles=(Reptiles) a;
				reptiles.describeReptiles();
			}
		}
	}
	
	

}
