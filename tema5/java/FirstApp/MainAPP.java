package tema5.java.FirstApp;

import java.util.ArrayList;
import java.util.Scanner;

public class MainAPP {
	  public static ArrayList<Animal> listaAn=new ArrayList<>();
	public static void main(String[] args) {
		Lion lion1=new Lion(3, "Leul1");
		Crocodile crocodile1=new Crocodile(5, "Crocodil1");
		Crocodile crocodile2=new Crocodile(2, "Crocodil2");
		Crocodile crocodile3=new Crocodile(1, "Crocodil3");
		Bears bears1=new Bears(2, "Ursul1");
		Bears bears2=new Bears(3, "Ursul2");
		Parrots parrots1=new Parrots(3, "Papagalul1");
		Lion lion2=new Lion(4, "Leul2");
		listaAn.add(bears1);
		listaAn.add(crocodile1);
		listaAn.add(parrots1);
		listaAn.add(lion1);
		listaAn.add(lion2);
		listaAn.add(bears2);
		listaAn.add(crocodile2);
		listaAn.add(crocodile3);
		
		Zoo zoo1=new Zoo(listaAn);
		zoo1.descriereAnimale();
		System.out.println("-------Print animals by input-----------");
		System.out.println("Introduceti ce doriti sa se afiseze:");
		Scanner sc1=new Scanner(System.in);
		 String Toate="ALL";
		 String Leu="Lion";
		 String Pasare="Parrot";
		 String Urs="Bear";
		 String Crocodil="Crocodile";
		 String input=sc1.next();
		
		afisareInFunctieDeTipAles(zoo1, Toate, Leu, Pasare, Urs, Crocodil, input);
		
		
		
		
		
	}
	private static void afisareInFunctieDeTipAles(Zoo zoo1, String Toate, String Leu, String Pasare, String Urs,
			String Crocodil, String input) {
		if(input.matches(Toate)) {
			zoo1.descriereAnimale();
		}
		if(input.matches(Leu)) {
			for(Animal a : listaAn) {
				if(a instanceof Lion) {
					Lion lion=(Lion) a;
					lion.describeMammals();
				}
			}
		}
		if(input.matches(Pasare)) {
			for(Animal a : listaAn) {
				if(a instanceof Parrots) {
					Parrots parrots=(Parrots) a;
					parrots.describeBirds();
				}
			}
		}
		if(input.matches(Urs)) {
			for(Animal a :listaAn) {
				if(a instanceof  Bears) {
					Bears bears=(Bears) a;
					bears.describeMammals();
					
					
				}
			}
		}
		if(input.matches(Crocodil)) {
			for(Animal a : listaAn) {
				if(a instanceof Crocodile) {
					Crocodile crocodile=(Crocodile) a;
				   crocodile.describeReptiles();
					
				}
			}
		}
	}

}
