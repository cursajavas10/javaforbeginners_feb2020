package tema5.java.FirstApp;

public class Crocodile  extends Animal implements Reptiles{

	public Crocodile(int age, String name) {
		super(age, name);
	}

	@Override
	public void describeReptiles() {
		System.out.println(name + " " + "are varsta de:" + age + " " + "  este reptila" + " " + " traieste doar in Asia");		
	}

}
