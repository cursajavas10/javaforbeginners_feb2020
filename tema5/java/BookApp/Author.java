package tema5.java.BookApp;

public class Author {
	
	String name;
	String surname;
	String dateofbirth;
	
	public Author(String name, String surname, String dateofbirth) {
		super();
		this.name = name;
		this.surname = surname;
		this.dateofbirth = dateofbirth;
	}

	@Override
	public String toString() {
		return "Author [name=" + name + ", surname=" + surname + ", dateofbirth=" + dateofbirth + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dateofbirth == null) ? 0 : dateofbirth.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((surname == null) ? 0 : surname.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Author other = (Author) obj;
		if (dateofbirth == null) {
			if (other.dateofbirth != null)
				return false;
		} else if (!dateofbirth.equals(other.dateofbirth))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (surname == null) {
			if (other.surname != null)
				return false;
		} else if (!surname.equals(other.surname))
			return false;
		return true;
	}
	
	 
	
	
	
	

}
