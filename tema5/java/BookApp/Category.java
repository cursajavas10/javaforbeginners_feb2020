package tema5.java.BookApp;

public class Category {

	String Categorie;
	Genres genres;
	
	public Category(String categorie, Genres genres) {
		super();
		Categorie = categorie;
		this.genres = genres;
	}

	@Override
	public String toString() {
		return "Category [Categorie=" + Categorie + ", genres=" + genres + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((Categorie == null) ? 0 : Categorie.hashCode());
		result = prime * result + ((genres == null) ? 0 : genres.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Category other = (Category) obj;
		if (Categorie == null) {
			if (other.Categorie != null)
				return false;
		} else if (!Categorie.equals(other.Categorie))
			return false;
		if (genres != other.genres)
			return false;
		return true;
	}
	
	
	
	
}
