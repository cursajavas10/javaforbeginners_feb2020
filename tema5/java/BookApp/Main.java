package tema5.java.BookApp;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
	public static ArrayList<Books> listaB = new ArrayList<>();
	public static String parolaAdministrator = "LibrariaMea16";
	public static void main(String[] args) {
		introducereCartiPentruCautare();
		System.out.println("Welcome to the library!");
		System.out.println(
				"Press 1 if u want to borrow a book or 2 if u are the administrator and want to register books:");
		Scanner scPornire = new Scanner(System.in);
		int numarPornire = scPornire.nextInt();
		if (numarPornire == 2) {
			System.out.println("Please introduce the passward for the administrator:");
			Scanner scParola = new Scanner(System.in);
			String parolaA = scParola.nextLine();
			if (parolaA.matches(parolaAdministrator)) {
				System.out.println("How many books do you want to register?");
				Scanner sc = new Scanner(System.in);
				int numarCartiRegister = sc.nextInt();
				inregistrareCarte(numarCartiRegister);
				System.out.println(listaB.toString());
			} else {
				System.out.println("Nu ai introdus bine parola!");
				System.exit(0);
			}
		} else if (numarPornire == 1) {
			System.out.println("Please introduce your name:");
			Scanner scNumeReader = new Scanner(System.in);
			String numeCititor = scNumeReader.nextLine();
			System.out.println(numeCititor + " " + " the  criteria for searching a book in our library is by:");
			System.out.println("1->Book's name");
			System.out.println("2->Author's name");
			System.out.println("3->Date");
			System.out.println("Please introduce the number for the criteria,after you want to search");
			Scanner scCriterieNumar = new Scanner(System.in);
			int numarCriterie = scCriterieNumar.nextInt();
			cautareDupaCriteriaSelectata(numarCriterie);
		} else {
			System.out.println("Please select 1 or 2 depending on your needs!");

		}
	}

	private static void cautareDupaCriteriaSelectata(int numarCriterie) {
		if (numarCriterie == 1) {
			System.out.println("Please introduce the name of the book:");
			Library lib1=new Library(listaB);;
			Scanner scCriteria1 = new Scanner(System.in);
			String numeCriterie1 = scCriteria1.nextLine();
			System.out.println("This is the result:");
			boolean Gasit1 = false;
			for (Books b : listaB) {
				if (b.name.matches(numeCriterie1)) {
					System.out.println(b.toString());
					Gasit1 = true;
					System.out.println("If you want to borrow it write OK");
					Scanner scannerBorrow=new  Scanner(System.in);
					String Borrow=scannerBorrow.next();
					if(Borrow.matches("OK")) {
					//lib1.BorrowBook(b);
					System.out.println(lib1.toString());
						
					}
				}
			}
			if (Gasit1 == false) {
				System.out.println("There is no book for what you have searching !");
			}

		} else if (numarCriterie == 2) {
			System.out.println("Please introduce the author's name:");
			Scanner scCriteria2 = new Scanner(System.in);
			String numeCriteria2 = scCriteria2.nextLine();
			System.out.println("This is the result:");
			boolean Gasit2 = false;
			for (Books b : listaB) {
				if (b.author.name.matches(numeCriteria2)) {
					System.out.println(b.toString());
					Gasit2 = true;
				}
			}
			if (Gasit2 == false) {
				System.out.println("There is no book for what you have searching !");
			}

		} else if (numarCriterie == 3) {
			System.out.println("The date for searching must have the format as DD.MM.YYYY !");
			System.out.println("Please introduce the date for searching:");
			Scanner scDataCrit = new Scanner(System.in);
			String DataCrti = scDataCrit.next();
			boolean Gasit3 = false;
			System.out.println("This is the result:");
			for (Books b : listaB) {
				if (b.date.matches(DataCrti)) {
					System.out.println(b.toString());
					Gasit3 = true;
				}
			}

			if (Gasit3 == false) {
				System.out.println("There is no book for what you have searching !");
			}
		}
	}

	private static void introducereCartiPentruCautare() {
		Author author1 = new Author("Vasile", "Alecsandrii", "20.05.1965");
		Author author2 = new Author("Mihai", "Eminescu", "19.07.1980");
		Author author3 = new Author("Ion", "Creanga", "13.10.1978");
		Genres genres1 = Genres.Action;
		Genres genres2 = Genres.drama;
		Genres genres3 = Genres.romance;
		Category category1 = new Category("kids", genres1);
		Category category2 = new Category("Blinds", genres3);
		Category category3 = new Category("Adults", genres2);
		Books books1 = new Books(20, "20.06.1978", author1, 3, category1, "Carte de actiune1");
		Books books2 = new Books(15, "23.02.1990", author2, 2, category2, "Carte de romance1");
		Books books3 = new Books(30, "19.03.1990", author2, 1, category1, "Carte de actiune2");
		Books books4 = new Books(40, "20.11.1978", author1, 5, category3, "Carte de drama1");
		Books books5 = new Books(34, "20.06.1978", author3, 6, category3, "Carte de drama2");
		listaB.add(books1);
		listaB.add(books2);
		listaB.add(books3);
		listaB.add(books4);
		listaB.add(books5);
	}

	private static void inregistrareCarte(int numarCartiRegister) {
		for (int i = 0; i < numarCartiRegister; i++) {
			System.out.println("Please introduce the name of the author:");
			Scanner scAuthor = new Scanner(System.in);
			String nameAuthor = scAuthor.next();
			System.out.println("Please introduce the surname of the author:");
			Scanner scSurname = new Scanner(System.in);
			String sournameAuthor = scSurname.nextLine();
			System.out.println("Please introduce the date of the birth author:");
			Scanner scDate = new Scanner(System.in);
			String date = scDate.nextLine();
			Author author = new Author(nameAuthor, sournameAuthor, date);
			System.out.println("Please introduce the name of the book:");
			Scanner scName = new Scanner(System.in);
			String nameBook = scName.nextLine();
			System.out.println("Please introduce the date of the book");
			Scanner scDateBOOK = new Scanner(System.in);
			String dateBook = scDateBOOK.next();
			System.out.println("Please introduce the price of the book:");
			Scanner scPrice = new Scanner(System.in);
			int PriceBook = scPrice.nextInt();
			System.out.println("Please introduce the quantity of this book:");
			Scanner scQuantity = new Scanner(System.in);
			int QuantityBooks = scQuantity.nextInt();
			System.out.println("Please introduce the category of the book for the public: ");
			Scanner scCategory = new Scanner(System.in);
			String categoryBook = scCategory.next();
			System.out.println("Please introduce the genres for the book:");
			Scanner scGenres = new Scanner(System.in);
			String GenresBook = scGenres.next();
			Genres genres = Genres.Action;
			genres = validareGenuri(GenresBook, genres);
			Category category = new Category(categoryBook, genres);
			Books books = new Books(PriceBook, dateBook, author, QuantityBooks, category, nameBook);
			listaB.add(books);
		}
	}

	private static Genres validareGenuri(String GenresBook, Genres genres) {
		try {
			genres = Genres.valueOf(GenresBook);
		} catch (Exception e) {
			System.out.println("You must choose between:" + java.util.Arrays.asList(Genres.values()));// toate
																										// elementele
																										// din arryay
			System.exit(0);
		}
		return genres;
	}

}
