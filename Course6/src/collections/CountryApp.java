package collections;

import java.util.Map;
import java.util.TreeMap;

public class CountryApp {

	public static void main(String[] args) {
		Country c1 = new Country("Romania", "Europa", 23000000);
		Country c2 = new Country("Mexic", "America", 55000000);
		Country c3 = new Country("Japonia", "Asia", 6000000);
		Country c4 = new Country("Bulgaria", "Europa", 5000000);
		Country c5 = new Country("Siria", "Africa", 88000000);
		
		TreeMap<String, Country> map = new TreeMap();

		map.put(c1.name, c1);
		map.put(c2.name, c2);
		map.put(c3.name, c3);
		map.put(c4.name, c4);
		map.put(c5.name, c5);
		
		System.out.println(map);
		/*for(Map.Entry<String, Country> m : map.entrySet()) {
			Country c = m.getValue();
			System.out.println(c);
		}*/
		
		Country c6 = new Country("Bulgaria", "Europa", 5000000);
		Country c7 = new Country("Australia", "Australia", 67000000);
		TreeMap<String, Country> map2 = new TreeMap();
		map2.put(c1.name, c1);
		map2.put(c2.name, c2);
		map2.put(c3.name, c3);
		map2.put(c4.name, c4);
		map2.put(c5.name, c5);
		map2.put(c6.name, c6);
		map2.put(c7.name, c7);
		System.out.println(map2);
		
		//map.entrySet().retainAll(map2.entrySet());
		//System.out.println(map);
		//map.entrySet().removeAll(map2.entrySet());
		//System.out.println(map);
		map.putAll(map2);
		System.out.println(map);
	}

}
