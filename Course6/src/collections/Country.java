package collections;

public class Country {
	String name;
	String continent;
	int population;
	
	public Country(String name, String continent, int population) {
		super();
		this.name = name;
		this.continent = continent;
		this.population = population;
	}

	@Override
	public String toString() {
		return "Country [name=" + name + ", continent=" + continent + ", population=" + population + "]";
	}
	
	
	
	
}
