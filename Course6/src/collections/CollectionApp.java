package collections;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CollectionApp {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		List<String> list = new ArrayList();
		
		while(true) {
			System.out.println("Please insert your name:");
			String name = sc.nextLine();
			if(name.equalsIgnoreCase("EXIT")) {
				break;
			}
			list.add(name);
		}
		System.out.println(list);
		
		System.out.println("Please insert the name to be removed:");
		String nameToRemove = sc.nextLine();
		if(!list.contains(nameToRemove)) {
			System.out.println("The name is not present!!!!!");
		} else {
			list.remove(nameToRemove);
			System.out.println(list);
		}
		
		System.out.println("Please insert the name to be replaced:");
		String nameToReplace = sc.nextLine();
		System.out.println("Please insert the new name:");
		String newName = sc.nextLine();
		int index = list.indexOf(nameToReplace);
		list.set(index, newName);
		System.out.println(list);
		
	}

}
