package exceptions;

import java.util.Scanner;

public class ExceptionApp {

	public static void main(String[] args)  {
		Scanner sc = new Scanner(System.in);
		System.out.println("Please insert a number:");
		try {
			long number = Long.parseLong(sc.nextLine());
			if(number > 2147483647) {
				throw new CustomException();
			}
			int intNumber = (int) number;
			System.out.println("You enterd the following number:" + intNumber);
		} catch (NumberFormatException nfe) {
			System.out.println("Your input is not a number!!!!");
		} catch (CustomException e) {
			System.out.println("WARNING! The number is too big!");
		}
		
		
		System.out.println("END");
	}

}
