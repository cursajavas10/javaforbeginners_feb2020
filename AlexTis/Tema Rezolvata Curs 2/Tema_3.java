package TemaCurs2;

import java.util.Scanner;

public class Tema_3 {
	public static void main(String argsp[]) {
		
		System.out.println("Please input your character:");
		Scanner sc = new Scanner(System.in);
		
		char character = sc.next().charAt(0);
		int ascii = (int) character;
		
		System.out.println("The ASCII number for "+character+" is "+ascii);
		
		sc.close();
	}
}
