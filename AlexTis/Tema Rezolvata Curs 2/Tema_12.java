package TemaCurs2;

import java.util.Scanner;

public class Tema_12 {
	public static void main(String args[]) {
		
		System.out.println("Please input your terms number below: ");
		
		Scanner sc = new Scanner(System.in);
		int terms = sc.nextInt(), sum = 0;
		System.out.print("The first "+terms+" odd numbers are: ");
		for(int i=1; i<terms*2; i+=2) {
			
			System.out.print(i+" ");
			sum+=i;
		}
		System.out.println("\nThe sum of the odd numbers is: "+sum);
		
		sc.close();
	}
}
