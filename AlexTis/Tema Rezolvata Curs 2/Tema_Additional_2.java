package TemaCurs2;

import java.util.Scanner;
import java.text.DecimalFormat;

public class Tema_Additional_2 {
	public static void main(String args[]) {
		
		System.out.println("Please insert your seconds below to be converted: ");
		Scanner sc = new Scanner(System.in);
		
		long seconds = sc.nextLong();
		
		float minutes = seconds/60f;
		float hours = seconds/3600f;
		float days = seconds/(3600*24f);
		float years = seconds/(3600*24*365f);
		
		DecimalFormat df = new DecimalFormat();
		df.setMaximumFractionDigits(2);
		
		System.out.println("Number of seconds: "+df.format(seconds));
		System.out.println("Number of minutes: "+df.format(minutes));
		System.out.println("Number of hours: "+df.format(hours));
		System.out.println("Number of days: "+df.format(days));
		System.out.println("Number of years: "+df.format(years));
		
		sc.close();
	}
}
