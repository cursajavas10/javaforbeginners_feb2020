package TemaCurs2;

public class Tema_Additional_6 {
	public static void main(String args[]) {
		
		String result = String.format("11223344 + 0.3 = %.10f",11223344 + 0.3);
		System.out.println(result);
		
		result = String.format("1111 + 1111.22 = %.0f",1111 + 1111.22);
		System.out.println(result);
		
		result = String.format("1 + 0.1234 = %.2f",1 + 0.1234);
		System.out.println(result);
	}
}
