package TemaCurs2;

import java.util.Scanner;

public class Tema_Additional_3 {
	public static void main(String args[]) {
		
		System.out.println("Please input your 5 digits number:");
		Scanner sc = new Scanner(System.in);
		
		CheckNumber(sc);
	}
	
	public static void CheckNumber(Scanner sc) {
		
		int Number = sc.nextInt();
		int length = (int) (Math.log10(Number) + 1);
		
		if(length == 5) {
			
	        int sum = 0;
	        while (Number > 0) {
	            sum = sum + Number % 10;
	            Number = Number / 10;
	        }
	        
	        System.out.println("The sum of your number is: "+sum);
	        sc.close();
	        
		}
		else {
			System.out.println("Invalid number! Please insert a 5 digits number: ");
			CheckNumber(sc);
		}
	}
}