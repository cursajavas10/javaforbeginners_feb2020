package TemaCurs2;

import java.util.Scanner;

public class Tema_5 {
	public static void main(String args[]) {
		
		System.out.println("Would you like to convert an 'Integer' or a 'Double' to Hex?");
		
		Scanner sc = new Scanner(System.in);	
		MethodChosen(sc);
	}
	
	public static void MethodChosen(Scanner sc) {
		
		String method = sc.nextLine();
		if(method.equalsIgnoreCase("integer")) {
			
			System.out.println("Please input your Integer below:");
			int number = sc.nextInt();
			System.out.println("Integer "+number+" in Hexadecimal is equal to: "+Integer.toHexString(number));
			sc.close();
		}
		else if(method.equalsIgnoreCase("double")) {
			
			System.out.println("Please input your Double below:");
			double dble = sc.nextDouble();
			System.out.println("Double "+dble+" in Hexadecimal is equal to: "+Double.toHexString(dble));
			sc.close();
		}
		else {
			
			System.out.println("Invalid input! Please choose between 'Integer' and 'Double'.");
			MethodChosen(sc);
		}
	}
}
