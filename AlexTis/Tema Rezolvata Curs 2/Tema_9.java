package TemaCurs2;

import java.util.Scanner;

public class Tema_9 {
	public static void main(String args[]) {
		
		int numbers[] = new int[5], sum=0;
		double average;
		
		System.out.println("Please input the 5 numbers below: ");
		for(int i=0; i<numbers.length; i++) {
			
			Scanner sc = new Scanner(System.in);
		    numbers[i] = sc.nextInt();
		    sum+=numbers[i];
		    
		    if(i == numbers.length-1)
		    	sc.close();
		}
		
		System.out.printf("Your numbers: %d, %d, %d, %d, %d", numbers[0],numbers[1],numbers[2],numbers[3],numbers[4]);
		System.out.printf("\nThe sum of the numbers: %d", sum);
		
		average=sum/numbers.length;
		System.out.printf("\nThe average of the numbers: %.02f", average);
	}
}
