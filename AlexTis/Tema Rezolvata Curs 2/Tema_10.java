package TemaCurs2;

import java.util.Scanner;

public class Tema_10 {
	public static void main(String args[]) {
		
		System.out.println("Please input your number below: ");
		Scanner sc = new Scanner(System.in);
		int number = sc.nextInt();
		
		String result = number >= 0 ? "Your number is positive" : "Your number is negative";
		System.out.println(result);
		
		sc.close();
	}
}
