package TemaCurs2;

import java.text.DecimalFormat;
import java.util.Scanner;

public class Tema_14 {
	public static void main(String args[]) {
		
		System.out.println("Please input the starting KM below: ");
		Scanner sc = new Scanner(System.in);
		GasCalulator(sc);
	}
	
	public static void GasCalulator(Scanner sc) {
		
		DecimalFormat df = new DecimalFormat();
		df.setMaximumFractionDigits(2);
		
		int start = sc.nextInt();
		double citycons = 0.0, highwaycons = 0.0;
	
		sc.reset();
		System.out.println("Please input the ending KM below: ");
		
		int end = sc.nextInt();
		if(end > start) {
			
			System.out.println("How many gas litres is your car consuming on 100 KM in the city? Input answer below: ");
			sc.reset();
			citycons = sc.nextDouble();
			System.out.println("How many gas litres is your car consuming on 100 KM in the highway? Input answer below: ");
			sc.reset();
			highwaycons = sc.nextDouble();
			
			sc.reset();
			System.out.println("Is your trip thru the city or highway? ( true = city, false = highway )");
			boolean way = sc.nextBoolean();
			
			
			System.out.println("Based on the information provided, the results are shown below:");
			System.out.println("Start KM: "+start);
			System.out.println("End KM: "+end);
			System.out.println("Total Trip KM: "+(end-start));
			System.out.println("Consumption in city: "+citycons);
			System.out.println("Consumption on the highway: "+highwaycons);
			System.out.println("Is the trip in the city? "+way);
			
			String result = way ? String.format("Gas required: %.1f Litres",(end-start)*citycons/100) : String.format("Gas required: %.1f Litres",(end-start)*highwaycons/100);
			System.out.println(result);
			
			sc.close();
			
		} 
		else {
			
			System.out.println("Error! Ending KM cannot be less or equal with starting KM! Please input again below: ");
			GasCalulator(sc);
		}
	}
}
