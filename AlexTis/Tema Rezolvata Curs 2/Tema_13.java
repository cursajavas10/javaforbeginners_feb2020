package TemaCurs2;

import java.util.Scanner;

public class Tema_13 {
	public static void main(String args[]) {
		
		System.out.println("Please input the number terms below: ");
		
		Scanner sc = new Scanner(System.in);
		int terms = sc.nextInt();
		
		for(int i=1; i<=terms; i++) {
			
			for(int j=1; j<=i; j++)
				System.out.print(j);
			
			System.out.println("");	
		}
		
		sc.close();
	}
}
