package TemaCurs2;

import java.util.Scanner;

public class Tema_Additional_5 {
	public static void main(String args[]) {
		
		System.out.println("Please input below the Hexagon side value: ");
		Scanner sc = new Scanner(System.in);
		
		double side = sc.nextDouble(), area = ((3*Math.sqrt(3))/2)*Math.pow(side, 2);
		
		System.out.printf("The area of a Hexagon with a %.2f side is: %.2f",side,area);
		
		sc.close();
	}
}
