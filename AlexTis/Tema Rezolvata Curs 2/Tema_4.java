package TemaCurs2;

import java.util.Scanner;

public class Tema_4 {
	public static void main(String args[]) {
		
		System.out.println("Please insert your string below:");
		
		Scanner sc = new Scanner(System.in);
		
		String result = sc.nextLine();
		
		System.out.println("The length of your string is: "+result.length()+" characters.");
		
		sc.close();
	}
}
