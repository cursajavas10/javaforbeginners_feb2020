package TemaCurs2;

import java.text.DecimalFormat;
import java.util.Scanner;

public class Tema_1 {
	public static void main(String args[]) {
		
		System.out.println("Please insert the value of the triangle base:");
		
		Scanner sc = new Scanner(System.in);
		float base = sc.nextFloat();
		
		sc.nextLine();
		
		System.out.println("Please insert the value of the triangle height:");
		float height = sc.nextFloat();
		
		float area=(base*height)/2;
		DecimalFormat df = new DecimalFormat();
		df.setMaximumFractionDigits(2);
		
		System.out.println("The area of the triangle is: "+df.format(area));
		
		sc.close();
	}
}
