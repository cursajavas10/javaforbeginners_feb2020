package TemaCurs2;

import java.text.DecimalFormat;
import java.util.Scanner;

public class Tema_Additional_1 {

	public static void main(String args[]) {
		
		System.out.println("Please insert your year(s) below to convert them in seconds: ");
		Scanner sc = new Scanner(System.in);
		
		short years = sc.nextShort();
		long seconds = (long) years*365*24*60*60;
		
		DecimalFormat df = new DecimalFormat();
		df.setMaximumFractionDigits(2);
		
		System.out.println("In "+df.format(years)+" year(s) we have "+df.format(seconds)+ " seconds.");
		
		sc.close();
	}
}
