package TemaCurs2;

import java.util.Scanner;

public class Tema_11 {
	public static void main(String args[]) {
		
		int numbers[] = new int[3], greatest=0;
		System.out.println("Please input your 3 numbers below: ");
		
		for(int i=0; i<numbers.length; i++) {
			
			Scanner sc = new Scanner(System.in);
		    numbers[i] = sc.nextInt();
		    
		    if(numbers[i] > greatest)
		    	greatest=numbers[i];
		    
		    if(i == numbers.length-1)
		    	sc.close();
		}
		
		System.out.printf("Out of %d, %d and %d, the greatest number is: %d", numbers[0], numbers[1], numbers[2], greatest);
	}
}
