package TemaCurs2;

import java.text.DecimalFormat;

public class Tema_2 {
	public static void main(String args[]) {
		
		byte small = 3;
		long big = 2147483647;
		
		long result = (long) big+small;
		
		DecimalFormat df = new DecimalFormat();
		df.setMaximumFractionDigits(2);
		
		System.out.println("The result is: "+df.format(result));
	}
}
