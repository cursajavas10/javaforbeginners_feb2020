package TemaCurs2;

import java.util.Scanner;

public class Tema_7 {
	public static void main(String args[]) {
		
		System.out.println("Please input your letter below: ");
		Scanner sc = new Scanner(System.in);
		CheckLetter(sc);
	}
	
	public static void CheckLetter(Scanner sc) { 
		
		String letter = sc.nextLine();
		if(letter.length() == 1) {
			
			char charletter = letter.charAt(0);
			
			if(charletter >= 65 && charletter <= 122) { // Working
				
				String result = charletter%2 == 0 ? "The letter '%s' is a Consonant" : "The letter '%s' is a Vowel";
				System.out.printf(result,charletter);
				
				sc.close();
			}
			else { // Error
				
				System.out.println("Invalid letter! Please try again below: ");
				CheckLetter(sc);
			}
		}
		else { // Error
			
			System.out.println("Invalid letter! Please try again below: ");
			CheckLetter(sc);
		}
	}
}
