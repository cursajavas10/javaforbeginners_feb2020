package TemaCurs1;

public class Tema4 {

	public static void main(String args[]) {
		
		String result;
		result = String.format("1) -5 + 8 * 6 = %d", -5+8*6);
		System.out.println(result);
		
		result = String.format("2) ( 55 + 9 ) %% 3 = %d rest %d", (55+9)/3, (55+9)%3);
		System.out.println(result);
		
		result = String.format("3) 20 -3 * 5 / 8 = %d rest %d", 20-3*5/8, 20-3*5%8);
		System.out.println(result);
		
		result = String.format("4) 5 + 15 / 3 * 2 -8 %% 3 = %d rest %d", 5+15/3*2-8/3, 5+15/3*2-8%3);
		System.out.println(result);
		
	}
}
