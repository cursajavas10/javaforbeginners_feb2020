package TemaCurs1;

public class Tema3 {
	
	public static void main(String args[]) {
		short x=150, y=20;
		int z=0;

		System.out.println("Numerele alese vor fi "+x+ " si "+y+".");
		z=x+y;
		System.out.println("Suma celor doua numere este: "+z);
		z=x-y;
		System.out.println("Diferenta celor doua numere este: "+z);
		z=x*y;
		System.out.println("Produsul celor doua numere este: "+z);
		z=x/y;
		System.out.println("Catul celor doua numere este: "+z);
		String text;
		text = String.format("Restul celor doua numere este: %d. %d=%d*%d+%d.", x%y,x,y,x/y,x%y);
		System.out.println(text);
	}
}
