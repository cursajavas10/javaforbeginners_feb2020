package TemaCurs1;

public class Tema5CityClass {
	
	public static String CommaNumber(int number) {
		
		byte counter=-1;
		String result = String.format("%d",number);
		
		for(int l=result.length(); l>0; l--) {
			
			counter++;
			if(counter == 3) {
				
				result = result.substring(0, l) + ',' + result.substring(l);
				counter=0;
			}
		}
		return result;
	}

	String CityName;
	int CityNoOfPeople;
	String CityDistrict;
	String CityCountry;
	String CityContinent;
	
	void describe() {
		
		System.out.println("In "+CityContinent+ " avem "+CityCountry+" cu capitala la "+CityName+" in judetul "+CityDistrict+". Capitala are o populatie de aproximativ "+CommaNumber(CityNoOfPeople)+" locuitori.");
	}
}
