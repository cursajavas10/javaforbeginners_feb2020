package TemaCurs1;

public class Tema5 {
	
	public static void main(String args[]) {
		
		Tema5CountryClass Tara = new Tema5CountryClass();
		Tara.CountryName="Romania";
		Tara.CountryContinent="Europa de Est";
		
		Tema5CityClass Oras = new Tema5CityClass();
		Oras.CityName="Bucuresti";
		Oras.CityNoOfPeople=2000000;
		Oras.CityDistrict="Ilfov";
		Oras.CityCountry= Tara.CountryName;
		Oras.CityContinent= Tara.CountryContinent;
		
		Oras.describe();
	}

}
