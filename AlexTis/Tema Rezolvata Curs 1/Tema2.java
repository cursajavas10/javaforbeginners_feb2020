package TemaCurs1;

public class Tema2 {
	
	public static String CommaNumber(int number) {
		
		byte counter=-1;
		String result = String.format("%d",number);
		
		for(int l=result.length(); l>0; l--) {
			
			counter++;
			if(counter == 3) {
				
				result = result.substring(0, l) + ',' + result.substring(l);
				counter=0;
			}
		}
		return result;
	}
	
	public static void main(String args[]) {
		
		City bucharest = new City();
		bucharest.Name = "Bucuresti";
		bucharest.Country = "Romania";
		bucharest.Population = 2000000;
		
		bucharest.describe();
	}
}

