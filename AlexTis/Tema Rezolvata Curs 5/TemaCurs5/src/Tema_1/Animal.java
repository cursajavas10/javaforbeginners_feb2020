package Tema_1;

public abstract class Animal {
	String name;
	int age;
	
	public Animal(String name, int age) {
		this.name = name;
		this.age = age;
	}

	public String toString() {
		return "Animal [name=" + name + ", age=" + age + "]";
	}
	
	public abstract void animalKind();
}
