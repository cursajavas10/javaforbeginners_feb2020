package Tema_1;

public class Parrot extends Animal {

	public Parrot(String name, int age) {
		super(name, age);
	}

	public void animalKind() {
		System.out.println(name + " is a Bird.");
	}
}
