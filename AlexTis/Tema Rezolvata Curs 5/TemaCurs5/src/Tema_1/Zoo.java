package Tema_1;

public class Zoo {
	
	Animal[] aList;
	
	public Zoo(Animal[] aList) {
		this.aList = aList;
	}
	
	void describe() {
		for(Animal i : aList) {
			if(i != null) {
				System.out.println(i);
				i.animalKind();
				if(i instanceof LivesInAsia) {
					((LivesInAsia) i).livesInAsia();
				}
			}
		}
	}
}
