package Tema_1;

public class Lion extends Animal implements LivesInAsia {

	public Lion(String name, int age) {
		super(name, age);
	}

	public void animalKind() {
		System.out.println(name + " is a Mammal.");
	}

	public void livesInAsia() {
		System.out.println(name + " lives in Asia.");
	}
}
