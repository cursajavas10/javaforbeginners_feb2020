package Tema_1;

public class Bear extends Animal {
	
	public Bear(String name, int age) {
		super(name, age);
	}

	public void animalKind() {
		System.out.println(name + " is a Mammal.");
	}
}
