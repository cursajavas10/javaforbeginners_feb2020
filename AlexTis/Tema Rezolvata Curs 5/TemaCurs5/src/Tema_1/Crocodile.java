package Tema_1;

public class Crocodile extends Animal implements LivesInAsia{

	public Crocodile(String name, int age) {
		super(name, age);
	}

	public void animalKind() {
		System.out.println(name + " is a Reptile.");
	}

	public void livesInAsia() {
		System.out.println(name + " lives in Asia.");
	}
}
