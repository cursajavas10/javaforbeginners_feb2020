package Tema_1;

public class Penguin extends Animal {

	public Penguin(String name, int age) {
		super(name, age);
	}

	public void animalKind() {
		System.out.println(name + " is a Bird.");
	}
	
}
