package Tema_2;

import java.util.Scanner;

public class ZooApp {
	
	static Animal[] aList = new Animal[100];
	static Zoo zoo;

	public static void main(String[] args) {

		Crocodile c = new Crocodile("Crocodil", "Asia", "Reptile", 3);
		Lion l = new Lion("Leu", "Asia", "Mammal", 5);
		Penguin p = new Penguin("Pinguin", "North Pole", "Bird", 7);
		
		aList[0] = c;
		aList[1] = l;
		aList[2] = p;
		
		zoo = new Zoo(aList);
		System.out.println("Please input below which animal you want to describe: ");
		Scanner sc = new Scanner(System.in);
		
		/*
		 * for(Animal i : aList) { if(i != null) System.out.println(i.name); }
		 */
		
		checkOption(sc);
		
		
	}
	
	public static void checkOption(Scanner sc) {
		
		try {
			
			String option = sc.nextLine();
			if(option.equalsIgnoreCase("ALL")) {
				
				zoo.describeAll();
				System.out.println("Thank you for using our aplication!");
				
				sc.close();
				
			} else {
				
				boolean founded = false;
				
				for(Animal i : aList) {
					if(i != null) {
						if(option.equalsIgnoreCase(i.name)) {
							
							i.describe();
							System.out.println("Thank you for using our aplication!");
							
							founded = true;
							
							sc.close();
							break;
						} 
					}
				}
				if(!founded) {
					System.out.println("Animal not found in Data Base. Please try again:");
					checkOption(sc);
				}
				
			}
			
			
		} catch (Exception e) {
			
			System.out.println("Animal not found in Data Base. Please try again:");
			return;
		}
		
	}
	
}
