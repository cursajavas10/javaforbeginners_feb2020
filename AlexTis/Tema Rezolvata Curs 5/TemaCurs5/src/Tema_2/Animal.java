package Tema_2;

public abstract class Animal {
	String name, origin, kind;
	int age;
	
	public Animal(String name, String origin, String kind, int age) {
		this.name = name;
		this.origin = origin;
		this.kind = kind;
		this.age = age;
	}
	
	void describe() {
		
		System.out.println("Name of the animal: " + name);
		System.out.println("Type: " + kind);
		System.out.println("Age: " + age + " year(s) old");
		System.out.println("Origin: " + origin);
	}
}
