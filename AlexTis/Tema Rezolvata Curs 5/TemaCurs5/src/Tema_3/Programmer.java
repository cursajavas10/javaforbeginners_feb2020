package Tema_3;

public class Programmer extends Employee implements Bonus {

	public Programmer(String name, String surname, int age, int salary, boolean leader, boolean manager) {
		super(name, surname, age, salary, leader, manager);
	}

	public void hasBonus() {
		System.out.println(name + " " + surname + " has a bonus of " + bonus + "�.");
	}
}
