package Tema_3;


<<<<<<< HEAD
public abstract class Employee implements Manager, Leader {
=======
public abstract class Employee implements Manager, Leader {		//TODO: folosind interfete nu poti sa spui ca un angajat este lead sau manager si altul nu. As sugera sa folosestri 2 variabile boolean.
>>>>>>> a0d7aa1695e9b4ecdf5549bc5178f349754b35af
	String name, surname;
	int age, salary;
	public boolean leader, manager;
	
	public Employee(String name, String surname, int age, int salary, boolean leader, boolean manager) {
		this.name = name;
		this.surname = surname;
		this.age = age;
		this.salary = salary;
		this.leader = leader;
		this.manager = manager;
	}
	
	final void describe() {
		
		System.out.println("______________________________________");
		System.out.println("Name: " + name);
		System.out.println("Surname: " + surname);
		System.out.println("Age: " + age + " years old");
		System.out.println("Salary: " + salary + "�");
	}

	public void isManager() {
		System.out.println(name + " " + surname + " is the Manager of the company.");
	}
	
	public void isLeader() {
		System.out.println(name +  " " + surname + " is one of the Leaders in the company.");
	}
}
