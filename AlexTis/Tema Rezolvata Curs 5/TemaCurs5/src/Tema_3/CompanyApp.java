package Tema_3;

import java.util.Scanner;

public class CompanyApp {
	
	static int totalLeaders = 0;
	static boolean hasManager = false;
	static Employee[] aList = new Employee[100];
	static Company kfc;
	
	public static void main(String args[]) {
		
		Programmer p = new Programmer("Alexandru", "Mihail", 31, 3000, false, false);
		DBProgrammer dbp = new DBProgrammer("Andreea", "Alexandra", 33, 3000, false, false);
		Analyst a1 = new Analyst("Maria", "Georgescu", 27, 1200, false, false);
		Analyst a2 = new Analyst("Andrei", "Popescu", 28, 1000, false, false);
		
		aList[0] = p;
		aList[1] = dbp;
		aList[2] = a1;
		aList[3] = a2;
		
		kfc = new Company(aList);
		
		for(Employee i : aList) {
			if(i != null) {
				if(i.manager) hasManager = true;
				if(i.leader) totalLeaders++; 
			}
		}
		
		System.out.println("What would you like to do?\n1. View Employee List\n2. Add a Leader\n3. Add a Manager\n4. Change Manager\n5. Exit App");
		Scanner sc = new Scanner(System.in);
		
		checkOption(sc);
	}
	
	
	public static void checkOption(Scanner sc) {
		try {
			
			int option = Integer.parseInt(sc.nextLine());
			
			if(option < 1 || option > 5) {
				
				System.out.println("Invalid option! Please try again!");
				checkOption(sc);
				
			} else {
				
				switch(option) {
				
					case 1: { 
						
						kfc.describe();
						
						System.out.println("What would you like to do?\n1. View Employee List\n2. Add a Leader\n3. Add a Manager\n4. Change Manager\n5. Exit App");
					
						checkOption(sc);
						break;
					}
					
					case 2: {
						
						if(totalLeaders < 3) {
							
							System.out.println("Please insert the name for the new Leader below: ");
							makeSupperior(sc, false);
							
						} else {
							
							System.out.println("Maximum Leaders reached!");
							System.out.println("What would you like to do?\n1. View Employee List\n2. Add a Leader\n3. Add a Manager\n4. Change Manager\n5. Exit App");

							checkOption(sc);
						}
						break;
					}
					
					case 3: {
						
						if(!hasManager) {
							
							System.out.println("Please insert the name for the new Manager below: ");
							makeSupperior(sc, true);
							
						} else {
							
							System.out.println("There is already a Manager assigned on this Company!");
							System.out.println("What would you like to do?\n1. View Employee List\n2. Add a Leader\n3. Add a Manager\n4. Change Manager\n5. Exit App");

							checkOption(sc);
						}
						break;
					}
					
					case 4: {
						
						if(hasManager) {
							
							System.out.println("Please insert the name for the new Manager below: ");
							makeSupperior(sc, true);
							
						} else {
							System.out.println("No manager has been found! Must create one first!");
							System.out.println("What would you like to do?\n1. View Employee List\n2. Add a Leader\n3. Add a Manager\n4. Change Manager\n5. Exit App");
							checkOption(sc);
						}
					}
					
					case 5: {
						
						System.out.println("Thank you for using the Employee Database App!");
						
						sc.close();
					}
				}
				
			}
			
		} catch (Exception e) {
			System.out.println("Invalid option! Please try again!");
			checkOption(sc);
		}
	}
	
	public static void makeSupperior(Scanner sc, boolean manager) {
		
		String inputname = sc.nextLine(), text;
		boolean founded = false;
	
		for(Employee i : aList) {
			if(i != null) {
				text = String.format("%s %s", i.name, i.surname);
				if(inputname.equalsIgnoreCase(text)) {
					if(!manager) {
						if(!i.leader || !i.manager) {
							i.leader = true;
							System.out.println("You've made " + text + " a Leader of the company!");
							totalLeaders++;
								
							System.out.println("What would you like to do?\n1. View Employee List\n2. Add a Leader\n3. Add a Manager\n4. Change Manager\n5. Exit App");
							checkOption(sc);	
							
							founded = true;
							
						} else {
							System.out.println(text + " is already a Leader or a Manager!");
							makeSupperior(sc, false);
						}
					} else {
						
						if(!i.manager) {
							
							i.manager = true;
							System.out.println("You've made " + text + " the Manager of the company!");
							hasManager = true;
							
							founded = true;
							
							if(i.leader) {
								i.leader = false;
								totalLeaders--;
							}
							
							for(Employee j: aList) {
								if(j != null) 
									if(j.manager && j != i) j.manager = false; 
							}
									
							System.out.println("What would you like to do?\n1. View Employee List\n2. Add a Leader\n3. Add a Manager\n4. Change Manager\n5. Exit App");
							checkOption(sc);		
						}
						else {
							
							System.out.println(text + " is already the Manager of the company!");
							makeSupperior(sc, false);	
						}
					}
					break;
				} 
			}
		}
		
		if(!founded) {
			System.out.println("Employee not found!");
			makeSupperior(sc, false);
		}
	}
}
