package Tema_3;

public class Company {
	Employee[] list;

	public Company(Employee[] list) {
		this.list = list;
	}
	
	void describe() {
		for(Employee i : list) {
			if(i != null) {
				
				i.describe();
				if(i instanceof Bonus) ((Bonus) i).hasBonus();
				if(i.leader) i.isLeader();
				if(i.manager) i.isManager(); 
				System.out.println("______________________________________");
			}
		}
	}
}
