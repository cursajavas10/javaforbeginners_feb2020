package Tema_3;


public class DBProgrammer extends Programmer implements Bonus {

	public DBProgrammer(String name, String surname, int age, int salary, boolean leader, boolean manager) {
		super(name, surname, age, salary, leader, manager);
	}
	
	public void hasBonus() {
		System.out.println(name + " " + surname + " has a bonus of " + bonus + "�.");
	}
}
