package Curs4Tema3;

public class BookApp {
	
	public static void main(String args[]) {
		
		/*Dog dog1 = new Dog("Labrador", 2);
		Cat cat1 = new Cat("Siamese", 3);
		Lion lion1 = new Lion(8);
		Book book = new Book(dog1, cat1, lion1);
		book.describeAllAnimals();
		
		Cat cat2 = new Cat("Sphinx", 4);
		Lion lion2 = new Lion(3);
		Book book1 = new Book(cat2, lion2);
		book1.describeAllAnimals();
		
		
		Cat cat3 = new Cat("Breedless", 5);
		Book book2 = new Book(cat3);
		book2.describeAllAnimals();
		*/
		
		Dog dog1 = new Dog("Labrador", 2);
		Cat cat1 = new Cat("Nasoala", 3);
		Cat cat2 = new Cat("De Strada", 3);
		Cat cat3 = new Cat("Sphinx", 3);
		Cat cat4 = new Cat("Birmanese", 3);
		
		Book book3 = new Book(dog1, cat1, cat2, cat3, cat4);
		book3.describeAllAnimals();
	}
}
