package Curs4Tema3;

import java.util.Objects;

public class Book {
	
	Dog dog;
	Cat cat;
	Lion lion;
	
	String text;
	
	static int animalLegs = 4;
	
	public Book(Dog dog, Cat cat, Lion lion) {
		super();
		this.dog = dog;
		this.cat = cat;
		this.lion = lion;
	}
	
	public Book(Cat cat, Lion lion) {
		super();
		this.cat = cat;
		this.lion = lion;
	}
	
	public Book(Cat cat) {
		super();
		this.cat = cat;
	}
	
	public Book(Object ... objects) {
		for(Object a : objects) {
			if(a instanceof Dog) {
				//this.dog = (Dog) a;
			}
		}
	}
	
	void describeAllAnimals() {
		
		if(!Objects.isNull(dog)) {
			
			text = String.format("Dog breed [%s] is %d years old and has %d legs.", dog.name, dog.age, animalLegs);
			System.out.println(text);
		}
		
		if(!Objects.isNull(cat)) {
			text = String.format("Cat breed [%s] is %d years old and has %d legs.", cat.name, cat.age, animalLegs);
			System.out.println(text);
		}
		
		if(!Objects.isNull(lion)) {
			text = String.format("Lion has no breed, is %d years old and has %d legs.", lion.age, animalLegs);
			System.out.println(text);
		}
	}
}
