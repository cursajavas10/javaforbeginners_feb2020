package Curs4Tema3;

import java.util.Objects;

public class BookEnchanted {
	
	Dog[] dog = new Dog[10];
	Cat[] cat = new Cat[10];
	Lion[] lion = new Lion[10];
	
	String text;
	
	static int animalLegs = 4;
	
	int Dogs = 0, Cats = 0, Lions = 0;
	
	public BookEnchanted(Object ... objects) {
		for(Object a : objects) {
			if(a instanceof Dog) {
				this.dog[Dogs] = (Dog) a;
				Dogs++;
			} else if(a instanceof Cat) {
				this.cat[Cats] = (Cat) a;
				Cats++;
			} else if(a instanceof Lion) {
				this.lion[Lions] = (Lion) a;
				Lions++;
			}
		}
	}
	
	void describeAllAnimals() {
		
		for(int i=0; i<Dogs; i++) {
			if(!Objects.isNull(dog[i])) {
				
				text = String.format("Dog breed [%s] is %d years old and has %d legs.", dog[i].name, dog[i].age, animalLegs);
				System.out.println(text);
			} else break;
		}
		
		for(int j=0; j<Cats; j++) {
			if(!Objects.isNull(cat[j])) {
				
				text = String.format("Cat breed [%s] is %d years old and has %d legs.", cat[j].name, cat[j].age, animalLegs);
				System.out.println(text);
			} else break;
		}
		
		for(int k=0; k<Lions; k++) {
			if(!Objects.isNull(lion[k])) {
				text = String.format("Lion has no breed, is %d years old and has %d legs.", lion[k].age, animalLegs);
				System.out.println(text);
			} else break;
		}
	}
}
