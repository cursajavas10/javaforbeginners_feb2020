package TemaCurs4;

import java.util.Scanner;

public class Tema_5 {
	public static void main(String args[]) {
		System.out.println("Please insert your number below:");
		Scanner sc = new Scanner(System.in);
		
		checkNumber(sc);
	}
	
	public static void checkNumber(Scanner sc) {
		
		try {

			int number = Integer.parseInt(sc.nextLine()), originalNo = number;
			if(number < 0) {
				System.out.println("Invalid number! Please try again:");
				checkNumber(sc);
			}
			
			String result = "";
			
			while(number > 0) {
					result = number%2 == 0 ? String.format("0%s", result) : String.format("1%s", result);
					number=number/2;
			}
			
			System.out.println("The binary conversion of " +originalNo+ " is: " +result);
			
		} catch (Exception e) {
			System.out.println("Invalid number! Please try again:");
			checkNumber(sc);
		}
	}
}
