package TemaCurs4;

import java.util.Scanner;

public class Tema_4 {
	public static void main(String args[]) {
		System.out.println("Please insert your number below:");
		Scanner sc = new Scanner(System.in);
		
		checkNumber(sc);
	}
	
	public static void checkNumber(Scanner sc) {
		
		try {
			
			int number = Integer.parseInt(sc.nextLine());
			if(number < 0) {
				System.out.println("Invalid number! Please try again:");
				checkNumber(sc);
			}
			
			boolean isPrime = true;
			for(int i=2; i<number; i++) {
				if(number % i == 0) isPrime = false;
			}
			
			String text = isPrime ? "The number is prime" : "The number is not prime";
			System.out.println(text);
			
			sc.close();
			
		} catch (Exception e) {
			System.out.println("Invalid number! Please try again:");
			checkNumber(sc);
		}
	}
}
