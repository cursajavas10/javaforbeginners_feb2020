package TemaCurs4;

import java.util.Scanner;

public class Tema_2 {
	public static void main(String args[]) {
		
		System.out.println("Please insert your encoded code below:");
		Scanner sc = new Scanner(System.in);
		
		String encoded = sc.nextLine();
		String[] arrOfStr = encoded.split("#");
		
		for(int i=1; i<arrOfStr.length; i++) {
			System.out.print((char) Integer.parseInt(arrOfStr[i]));
		}
		sc.close();
	}
}
