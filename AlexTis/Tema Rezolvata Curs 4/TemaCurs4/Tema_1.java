package TemaCurs4;

import java.util.Scanner;

public class Tema_1 {
	public static void main(String args[]) {
		
		System.out.println("Please insert your code below:");
		Scanner sc = new Scanner(System.in);
		
		String pass = sc.nextLine(), result = "";
		
		for(int i=0; i<pass.length(); i++) {
			result = String.format("%s#%d", result, (int) pass.charAt(i));
		}
		
		System.out.println("Encoded code is: " + result);
		
		sc.close();
	}
}
