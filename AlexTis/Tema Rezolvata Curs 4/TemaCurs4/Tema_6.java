package TemaCurs4;

import java.util.Scanner;

public class Tema_6 {
	public static void main(String args[]) {
	
		System.out.println("Please choose your option:\n1. Matrix Calculator\n2. Animal Management\n3. Text Encoder\n4. Text Decoder.");
		Scanner sc = new Scanner(System.in);
		
		checkOption(sc);
	}
	
	public static void checkOption(Scanner sc) {
		try {
			int option = Integer.parseInt(sc.nextLine());
			if(option < 1 || option > 4) {
				System.out.println("Invalid option! Please try again!");
				checkOption(sc);
			}
			
			String[] arguments  = {"..."};
			
			switch(option) {
			
				case 1: TemaCurs3.Tema_3.main(arguments); break;
				case 2: Curs4Tema3.BookApp.main(arguments); break;
				case 3: TemaCurs4.Tema_1.main(arguments); break;
				case 4: TemaCurs4.Tema_2.main(arguments); break;
				
			}
			
			sc.close();
			
		} catch (Exception e) {
			System.out.println("Invalid option! Please try again!");
			checkOption(sc);
		}
	}
}
