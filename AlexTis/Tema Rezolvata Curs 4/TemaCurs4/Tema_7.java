package TemaCurs4;

import java.util.ArrayList;
import java.util.Scanner;

public class Tema_7 {
	
	static double currentDisciplineGrade = 0.0;
	static int currentDisciplineTotalGrades = 0;
	
	static double graduationGrade = 0.0;
	static int graduationTotalDisciplines = 0;
	
	static String text;
	
	static ArrayList<String> eachDiscipline = new ArrayList<String>();
	
	public static void main(String args[]) {
		
		System.out.println("Welcome Student! Input your grades for the first discipline.\nType 'NEXT' to go for the second discipline.\nType 'FINISH' to end.");
		Scanner sc = new Scanner(System.in);
		
		checkDisciplineGrades(sc);
	}
	
	public static void checkDisciplineGrades(Scanner sc) {
		
		String option = sc.nextLine();
		if(option.equalsIgnoreCase("NEXT")) {
			
			text = String.format("Average for Discipline no. %d: %.2f", graduationTotalDisciplines+1, currentDisciplineGrade/currentDisciplineTotalGrades);
			System.out.println(text);
			eachDiscipline.add(text);
			
			System.out.println("Please input the next discipline grades below:");
			
			graduationGrade += currentDisciplineGrade/currentDisciplineTotalGrades;
			graduationTotalDisciplines++;
			
			currentDisciplineGrade = 0.0;
			currentDisciplineTotalGrades = 0;
			
			checkDisciplineGrades(sc);
			
		} else if(option.equalsIgnoreCase("FINISH")) {
			
			if(currentDisciplineTotalGrades != 0) {
			
			text = String.format("Average for Discipline no. %d: %.2f", graduationTotalDisciplines+1, currentDisciplineGrade/currentDisciplineTotalGrades);
			System.out.println(text);
			eachDiscipline.add(text);
			
			graduationGrade += currentDisciplineGrade/currentDisciplineTotalGrades;
			graduationTotalDisciplines++;
			
			} 
			
			System.out.println("________________________________________________________________");
			for(String i : eachDiscipline) System.out.println(i);
			
			text = String.format("Graduation Grade: %.2f", graduationGrade/graduationTotalDisciplines);
			System.out.println(text);
			
			sc.close();
			
		} else {
			try {
				if(Integer.parseInt(option) < 4 || Integer.parseInt(option) > 10) {
				
					System.out.println("Grades cannot be lower than 4 or higher than 10! Please try again:");
					checkDisciplineGrades(sc);
					return;
				}
				
				currentDisciplineGrade+=Double.parseDouble(option);
				currentDisciplineTotalGrades++;
				System.out.println("Grade added! Please enter the next grade or one of the options below:");
				checkDisciplineGrades(sc);
				
			} catch (Exception e) {
				System.out.println("Invalid grade! Please try again:");
				checkDisciplineGrades(sc);
				return;
			}
		}
	}
}
