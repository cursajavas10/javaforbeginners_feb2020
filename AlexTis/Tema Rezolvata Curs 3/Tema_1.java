package TemaCurs3;

import java.util.ArrayList;
import java.util.Scanner;

public class Tema_1 {
	public static void main(String args[]) {
		
		ArrayList<String> names = new ArrayList<String>();
		ArrayList<Integer> ages = new ArrayList<Integer>();
		
		boolean isExited = false; // Used for loop
		String option; // Name & EXIT
		int age; // Age
		
		System.out.println("Welcome! Please tell me your name:");
		
		Scanner sc = new Scanner(System.in);
		
		while(!isExited) { // Loop
			
			option = sc.nextLine();
			if(!option.equalsIgnoreCase("EXIT")) {
				
				System.out.printf("Hello %s! Please input your age below:", option);
				
				age = sc.nextInt();
				System.out.printf("Thank you for your registration, %s!", option);
				
				
				names.add(option); // Adding Name to Array List
				ages.add(age); // Adding Age to Array List
				
				System.out.println("\nWelcome! Please tell me your name:");
				option = sc.nextLine();
				
			} else { // Print results
				
				System.out.printf("Below you can find a list with %d registered person(s):", ages.size());
				
				System.out.println("\n\nName\t\tAge");
				for(int i=0; i<ages.size(); i++) {
					System.out.printf("\n%s\t\t%d", names.get(i), ages.get(i));
				}
				
				isExited = true; // Stop the Loop
				sc.close();
			}
		}
	}
}
