package TemaCurs3;

import java.util.Scanner;

public class Tema_2 {
	public static void main (String arg[]) {               
		String[] words = {"This", "is", "a", "Java", "scramble", "game."};    
			
		System.out.print("The scrambled phrase is: ");           
		System.out.printf("%s %s %s %s %s %s", words[4], words[0], words[2], words[3], words[1], words[5]);
		Scanner in = new Scanner(System.in);   
		System.out.println("\nWhich is the first word from the phrase:" );
			
		String firstWord = in.nextLine();   
		System.out.println("You typed: " + firstWord);
		String result = firstWord.equals("This") ? "You Won!" : "You Lost!"; 
		System.out.print(result);
			
		in.close();
	}
}
