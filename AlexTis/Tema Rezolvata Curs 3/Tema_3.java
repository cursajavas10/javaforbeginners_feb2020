package TemaCurs3;

import java.util.Random;
import java.util.Scanner;

public class Tema_3 {
	
	static int columns, rows, constant, userOption;
	static boolean working = true;
	
	public static void main(String args[]) {
		
		System.out.println("Please insert the number of columns for the 2 matrixes: ");
		Scanner sc = new Scanner(System.in);
		Random rand = new Random();
		
		createMatrixColumn(sc);
		createMatrixRow(sc);
		
		int matrixA[][] = new int[columns][rows];
		int matrixB[][] = new int[columns][rows];
		int matrixC[][] = new int[columns][rows];
		int matrixD[][] = new int[columns][rows];
		
		for(int i=0; i<rows; i++) {
			for(int j=0; j<columns; j++) {
	
				matrixA[i][j] = rand.nextInt(100)+1;
				matrixB[i][j] = rand.nextInt(100)+1;
			}
		}
		
		printMatrix(matrixA);
		System.out.println();
		printMatrix(matrixB);
		
		while(working) {
			
			checkUserOperation(sc);
			
			switch(userOption) {
				
				// Adding
				case 1: {
					for(int i=0; i<rows; i++) {
						for(int j=0; j<columns; j++) {
							matrixC[i][j] = matrixA[i][j]+matrixB[i][j];
						}
					}
					printMatrix(matrixC);
					
					resetMatrix(matrixC);
					break;
				}
				
				// Negative
				case 2: {
					for(int i=0; i<rows; i++) {
						for(int j=0; j<columns; j++) {
							matrixC[i][j] = -matrixA[i][j];
							matrixD[i][j] = -matrixB[i][j];
						}
					}
					printMatrix(matrixC);
					System.out.println();
					printMatrix(matrixD);
					
					resetMatrix(matrixC);
					resetMatrix(matrixD);
					break;
				}
				
				// Subtract
				case 3: {
					for(int i=0; i<rows; i++) {
						for(int j=0; j<columns; j++) {
							matrixC[i][j] = matrixA[i][j]-matrixB[i][j];
						}
					}
					printMatrix(matrixC);
					
					resetMatrix(matrixC);
					break;
				}
				
				// Multiply by a Constant
				case 4: {
					for(int i=0; i<rows; i++) {
						for(int j=0; j<columns; j++) {
							matrixC[i][j] = constant*matrixA[i][j];
							matrixD[i][j] = constant*matrixB[i][j];
						}
					}
					printMatrix(matrixC);
					System.out.println();
					printMatrix(matrixD);
					
					resetMatrix(matrixC);
					resetMatrix(matrixD);
					constant = 0;
					break;
				}
				
				// Determinant
				case 5: {
					if(rows != columns) {
						checkUserOperation(sc);
						
					} else {
						
						System.out.println();
						System.out.println("Determinant for first Matrix: " + determinant(matrixA));
						System.out.println("Determinant for second Matrix: " + determinant(matrixB));
						System.out.println();
							
					}
					break;
				}
				
				// Exit
				case 6:
				{
					working = false;
					break;
				}
				default: {
					checkUserOperation(sc);
					break;
				}
			}
		}
		
		if(!working) {
			
			System.out.println("Thank you for using the Matrix Calculator!");
			sc.close();
		}
	}
	
	public static void createMatrixColumn(Scanner sc) {
		
		int insertedColumns = Integer.parseInt(sc.nextLine());
		if(insertedColumns < 2) {
			
			System.out.println("Matrix columns value should be atleast 2! Insert a new one: ");
			createMatrixColumn(sc);
		} else {
			
			columns = insertedColumns;
			System.out.println("Please insert the number of rows the 2 Matrixes should have: ");
		}
	}
	
	public static void createMatrixRow(Scanner sc) {
		
		int insertedRows = Integer.parseInt(sc.nextLine());
		if(insertedRows < 2) {
			
			System.out.println("Matrix rows value should be atleast 2! Insert a new one: ");
			createMatrixColumn(sc);
		} else {
			
			rows = insertedRows;
			System.out.println("Successfully created 2 " + columns + "x" + rows + " Matrixes");
		}
	}
	
	public static void printMatrix(int matrix[][]) {
		for(int i=0; i<matrix.length; i++) {
			for(int j=0; j<matrix[1].length; j++) {
				System.out.print(matrix[i][j] + "\t");
			}
			System.out.println();
		}
	}
	
	public static void resetMatrix(int matrix[][]) {
		for(int i=0; i<matrix.length; i++) {
			for(int j=0; j<matrix[1].length; j++) {
				matrix[i][j] = 0;
			}
		}
	}
	
	public static void checkUserOperation(Scanner sc) {
		
		System.out.println("Insert your operation: ADD, NEGATIVE, SUBTRACT, MULTIPLY, DETERMINANT, EXIT");
		String operation = sc.nextLine();
		
		if(operation.equalsIgnoreCase("ADD")) {
			System.out.println("You've chosen to ADD your Matrixes. Result is below: ");
			userOption = 1;
		}
		else if(operation.equalsIgnoreCase("NEGATIVE")) {
			System.out.println("You've chosen to NEGATIVE your Matrixes. Result is below: ");
			userOption = 2;
		}
		else if(operation.equalsIgnoreCase("SUBTRACT")) {
			System.out.println("You've chosen to SUBTRACT your Matrixes. Result is below: ");
			userOption = 3;
		}
		else if(operation.equalsIgnoreCase("MULTIPLY")) {
			System.out.println("You've chosen to MULTIPLY by a constant your Matrixes. Input the constant below: ");
			constant = Integer.parseInt(sc.nextLine());
			userOption = 4;
		}
		else if(operation.equalsIgnoreCase("DETERMINANT")) {
			System.out.println("You've chosen to display the DETERMINANT of your Matrixes. Result is below: ");
			userOption = 5;
		}
		else if(operation.equalsIgnoreCase("EXIT")) {
			System.out.println("You've chosen to EXIT!");
			userOption = 6;
		}
		else {
			System.out.println("Unknown operation! Please try again!");
			checkUserOperation(sc);
		}
	}
	
	public static int determinant(int matrix[][]) {
		int result = 0;
		
		if(rows == 2) {
			result = matrix[0][0]*matrix[1][1] - matrix[1][0]*matrix[0][1];
		} else {
			
			
		}
		return result;
	}
}
