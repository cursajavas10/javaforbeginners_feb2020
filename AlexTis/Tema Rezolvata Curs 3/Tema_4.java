package TemaCurs3;

import java.util.Scanner;

public class Tema_4 {	//TODO: !!! Chiar daca functioneaza, trebuie sa te gandesti in avans. Peste 6 luni vrei sa trecit de la un joc de 3/3 la 5/5. Cum poti face asta? Ar trebuie sa creezi un algoritm mai generic.
	
	static int GameEnded = 0;
	
	public static void main(String args[]) {
	
		Scanner sc = new Scanner(System.in);
		System.out.println("Welcome to a simple game of Tic-tac-toe!");
		
		char board[][] = new char[3][3];
		for(int i=0; i<board[1].length; i++) {
			for(int j=0; j<board[1].length; j++) {
				board[i][j] = '-';
				System.out.print(board[i][j] + "   ");
			}
			System.out.println(" ");
		}
		
		boolean playerTurn = false;
		
		for(int i=1; i<(board.length*board[1].length)+1; i++) {
				if(GameEnded > 0) {
					String winner = GameEnded == 1 ? "Player 1 won the game!" : "Player 2 won the game!";
					System.out.println(winner);
					break;
				}
				
			playerTurn = i % 2 == 0;
			gameCheck(playerTurn, sc, board);
		}
		
		if(GameEnded == 0) System.out.println("The game has ended with a draw!");
	}
	
	public static void gameCheck(boolean Player, Scanner sc, char board[][]) {
		
		int column, row;
		if(Player) {
			System.out.println("Player 2's turn is now! Insert the column place the 0:");
			column = sc.nextInt();
			sc.nextLine();
			System.out.println("Player 2's turn is now! Insert the row place the 0:");
			row = sc.nextInt();
		} else {
			System.out.println("Player 1's turn is now! Insert the column place the X:");
			column = sc.nextInt();
			System.out.println("Player 1's turn is now! Insert the row place the X:");
			row = sc.nextInt();
			
		}
		
		if(column >= board.length || row >= board[1].length) {
			System.out.println("Invalid position! Please try again by inserting the column: ");
			for(int i=0; i<board[1].length; i++) {
				for(int j=0; j<board[1].length; j++) {
					System.out.print(board[i][j] + "   ");
				}
				System.out.println(" ");
			}
			gameCheck(Player, sc, board);
		}
			
		if(Character.compare(board[column][row],'-') == 0) {
			board[column][row] = Player ? '0' : 'X';
			
			for(int i=0; i<board.length; i++) {
				for(int j=0; j<board[1].length; j++) {
					System.out.print(board[i][j] + "   ");
				}
				System.out.println(" ");
			}
			
			for(int i=0; i<board.length; i++) {
				if(Character.compare(board[i][0], board[i][1]) == 0 && Character.compare(board[i][1], board[i][2]) == 0) {
					if(Character.compare(board[i][0], 'X') == 0) GameEnded = 1;
					else if(Character.compare(board[i][0], '0') == 0) GameEnded = 2;
				}
			}
			
			for(int i=0; i<board[1].length; i++) {
				if(Character.compare(board[0][i], board[1][i]) == 0 && Character.compare(board[1][i], board[2][i]) == 0) {
					if(Character.compare(board[0][i], 'X') == 0) GameEnded = 1;
					else if(Character.compare(board[0][i], '0') == 0) GameEnded = 2;
				}
			}
			
			if(Character.compare(board[0][0], board[1][1]) == 0 && Character.compare(board[1][1], board[2][2]) == 0) {
				if(Character.compare(board[1][1], 'X') == 0) GameEnded = 1;
				else if(Character.compare(board[1][1], '0') == 0) GameEnded = 2;
			}
			
			if(Character.compare(board[0][2], board[1][1]) == 0 && Character.compare(board[1][1], board[2][0]) == 0) {
				if(Character.compare(board[1][1], 'X') == 0) GameEnded = 1;
				else if(Character.compare(board[1][1], '0') == 0) GameEnded = 2;
			}
			
		} else {
			System.out.println("Invalid position! Please try again by inserting the column: ");
			for(int i=0; i<board[1].length; i++) {
				for(int j=0; j<board[1].length; j++) {
					System.out.print(board[i][j] + "   ");
				}
				System.out.println(" ");
			}
			gameCheck(Player, sc, board);
		}
	}
}
