package TemaCurs3;

import java.util.Scanner;

public class Tema_4_Better {
	
	static int gameEnded = 0, value = 0;	// de preferat sa declaram cele 2 variabile pe 2 randuri.
	
	public static void main(String args[]) {
	
		Scanner sc = new Scanner(System.in);
		System.out.println("Welcome to a simple game of Tic-tac-toe!");
		System.out.println("Please insert below the number of columns and rows for the table: ");
		
		tableCheck(sc);
		char board[][] = new char[value][value];
		
		for(int i=0; i<board.length; i++) {
			for(int j=0; j<board[1].length; j++) {
				board[i][j] = '-';
				System.out.print(board[i][j] + "   ");
			}
			System.out.println(" ");
		}
		
		boolean playerTurn = false;
		
		for(int i=1; i<(board.length*board[1].length)+1; i++) {
				if(gameEnded > 0) {
					String winner = gameEnded == 1 ? "Player 1 won the game!" : "Player 2 won the game!";
					System.out.println(winner);
					break;
				}
				
			playerTurn = i % 2 == 0;
			gameCheck(playerTurn, sc, board);
		}
		
		if(gameEnded == 0) System.out.println("The game has ended with a draw!");
	}
	
	public static void tableCheck(Scanner sc) {
		
		int noColumnRow = sc.nextInt();
		if(noColumnRow < 3) {
			// randul acesta gol este inutil. Trebuie sters
			System.out.println("The table layout must have atleast 3 columns and 3 rows!");
			tableCheck(sc);
		} else {
			// randul acesta gol este inutil. Trebuie sters
			System.out.println("Successfully created a " + noColumnRow + "x" + noColumnRow + " table!");
			sc.nextLine();
			value = noColumnRow;
		}
	}
	
	public static void gameCheck(boolean Player, Scanner sc, char board[][]) {	//parametrul Player trebuie cu litera mica. Doar numele clasei se scrie cu litera mare!!!
		
		int column, row;
		if(Player) {
			System.out.println("Player 2's turn is now! Insert the column place the 0:");
			column = sc.nextInt();
			sc.nextLine();
			System.out.println("Player 2's turn is now! Insert the row place the 0:");
			row = sc.nextInt();
		} else {
			System.out.println("Player 1's turn is now! Insert the column place the X:");
			column = sc.nextInt();
			System.out.println("Player 1's turn is now! Insert the row place the X:");
			row = sc.nextInt();
			// randul acesta gol este inutil. Trebuie sters
		}
		
		if(column >= board.length || row >= board[1].length) {
			System.out.println("Invalid position! Please try again by inserting the column: ");
			for(int i=0; i<board[1].length; i++) {
				for(int j=0; j<board[1].length; j++) {
					System.out.print(board[i][j] + "   ");
				}
				System.out.println(" ");
			}
			gameCheck(Player, sc, board);
			return;
		}
			
		if(Character.compare(board[column][row],'-') == 0) {
			board[column][row] = Player ? '0' : 'X';	// super!! imi place!
			
			for(int i=0; i<board.length; i++) {
				for(int j=0; j<board[1].length; j++) {
					System.out.print(board[i][j] + "   ");
				}
				System.out.println(" ");
			}
			
			
			// Check Winner
			
			// Rows
			int winner = 0;
			
			for(int i=0; i<board.length; i++) {
				for(int j=0; j<board[1].length; j++) {
					if(Character.compare(board[i][j], 'X') == 0) {	// puteai sa compari mai simplu astfel: board[i][j] == 'X'. Un caracter reprezinta un numar.
						// randul acesta gol este inutil. Trebuie sters
						winner++;
						if(winner == board.length) {
							gameEnded = 1;
							break;
						}
					}
					else if(Character.compare(board[i][j], '0') == 0) {
						// randul acesta gol este inutil. Trebuie sters
						winner--;
						if(winner == -board.length) {
							gameEnded = 2;
							break;
						}
					}
				}		
			}
			
			// Columns
			winner = 0;
			
			for(int i=0; i<board.length; i++) {
				for(int j=0; j<board[1].length; j++) {
					if(Character.compare(board[j][i], 'X') == 0) {
						// randul acesta gol este inutil. Trebuie sters
						winner++;
						if(winner == board.length) {
							gameEnded = 1;
							break;
						}
					}
					else if(Character.compare(board[j][i], '0') == 0) {
						// randul acesta gol este inutil. Trebuie sters
						winner--;
						if(winner == -board.length) {
							gameEnded = 2;
							break;
						}
					}
				}
			}
			
			// Diagonals
			winner = 0;
			for(int i=0; i<board.length; i++) {
				if(Character.compare(board[i][i], 'X') == 0) {
					
					winner++;
					if(winner == board.length) {
						gameEnded = 1;
						break;
					}
				} else if(Character.compare(board[i][i], '0') == 0) {
					
					winner--;
					if(winner == board.length) {
						gameEnded = 2;
						break;
					}
				}
			}
			
			winner = 0;
			for(int i=board.length-1; i >=0; i--) {
				if(Character.compare(board[i][i], 'X') == 0) {
					
					winner++;
					if(winner == board.length) {
						gameEnded = 1;
						break;
					}
				} else if(Character.compare(board[i][i], '0') == 0) {
					
					winner--;
					if(winner == board.length) {
						gameEnded = 2;
						break;
					}
				}
			}
			
		} else {
			System.out.println("Invalid position! Please try again by inserting the column: ");
			for(int i=0; i<board[1].length; i++) {
				for(int j=0; j<board[1].length; j++) {
					System.out.print(board[i][j] + "   ");
				}
				System.out.println(" ");
			}
			
			gameCheck(Player, sc, board);
		}
	}
}
